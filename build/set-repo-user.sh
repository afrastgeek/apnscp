#!/bin/bash

# Utility script to set target user in git submodule/repo configuration
test -f /etc/sysconfig/apnscp && . /etc/sysconfig/apnscp

USER=${USER:-$(whoami)}
APNSCP_ROOT="${APNSCP_ROOT:-"$(realpath "$(dirname "$(realpath "$0")")"/../)"}"

pushd "$APNSCP_ROOT" 2> /dev/null || exit
trap 'popd' 0

# shellcheck disable=SC2016
git submodule foreach --recursive --quiet 'echo $name' | while read -r submodule ; do
  URL=$(git config --get --file=.gitmodules submodule."$submodule".url)
  [[ $URL != ssh:* ]] && continue
  [[ $URL == ssh://*@* ]] && continue
  git config --file=.gitmodules submodule."$submodule".url "ssh://root@${URL##*//}"
done

git submodule sync --recursive

# Non-submodules merged in
find . \( -path './vendor' \) -prune -o -name '.git' -type d -print | while read -r DIR; do
  pushd "$DIR" || continue
  for REMOTE in $(git remote show); do
    URL=$(git config --get remote."$REMOTE".url)
    [[ $URL != ssh:* ]] && continue
    [[ $URL == ssh://*@* ]] && continue
    git config remote."$REMOTE".url "ssh://root@${URL##*//}"
  done
  # shellcheck disable=SC2164
  popd
done
