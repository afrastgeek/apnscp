#!/usr/bin/env python
import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "jsonpickle"))
import pprint
import cPickle
import marshal
import jsonpickle
from types import StringType

if len(sys.argv) != 2:
	print >> sys.stderr, "Invalid usage"
	sys.exit(1)

filename = sys.argv[1]
if filename.endswith('.db'):
	load = marshal.load
elif filename.endswith('.pck'):
	load = cPickle.load
else:
	print >> sys.stderr, "Invalid usage. Expects .db or .pck"
	sys.exit(1)

fp = open(filename)
pp = pprint.PrettyPrinter(indent=4)
m=[]
try:
	cnt = 1
	while True:
		try:
			obj = load(fp)
		except EOFError:
			break

		if isinstance(obj, StringType):
			obj = json.loads(obj)
		else:
			obj = jsonpickle.encode(obj)
		cnt += 1
		m.append(obj)
finally:
	fp.close()

print obj
