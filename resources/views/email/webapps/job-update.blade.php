@if ($jobs->success())
@component('email.indicator', ['status' => 'success'])
    Your applications are now secure.
@endcomponent
@else
@component('email.indicator', ['status' => 'error'])
    Some applications failed to update!
@endcomponent
@endif
@component('mail::message')

{{-- Body --}}
# Howdy!

One or more applications have been successfully updated on your site. Below summarizes the changes,

@component('mail::table')
| | Location | Type | Old | New |
|-|:---------|:-----|:----|:----|
@foreach ($jobs as $job)
| <span class="@if (!$job->hasErrors()) success @else error @endif"> @if (($assurance = $job->getCandidate()->getAssurance()) && $assurance->assert())
    ⭐ @endif {{ $job->getStatusIcon() }} </span> | {{ $job->getCandidate()->getBaseUri() }} | {{ ucwords($job->getCandidate()->getAppType()) }} | {{ $job->getCandidate()->getVersion() }} | {{ $job->getCandidate()->getNextVersion() ?? '&ndash;'}} |
@foreach ($job->getLog(\Error_Reporter::E_ERROR|\Error_Reporter::E_WARNING|\Error_Reporter::E_EXCEPTION) as $log)
|<td colspan="5">**{!! \Error_Reporter::errno2str($log['severity']) !!}:** {!! $job->escapeMarkdown(substr(str_replace(["\r\n", "\n", "\r"], ' ', $log['message']), 0, 256)) !!}</td>
@endforeach
@endforeach
@endcomponent
⭐ <i>indicates a job that has passed <b>Update Assurance</b></i><br /> <br />

## Update Assurance
Update Assurance provides secondary validation that checks page output for consistency during scheduled updates and
rollbacks if something goes wrong. Update Assurance may be enabled for each Web App within {{ PANEL_BRAND }}. Here's how:

* Login to [{{PANEL_BRAND}}]({{ \Auth_Redirect::getPreferredUri() }})
* Go to **Web** > **Web Apps**
* Find the application, click **Select**
* Under _Actions_, click **Enable Snapshots**

@if (!$jobs->success())
## Note on failures
One or more jobs have failed. @if ($job->getCandidate()->willRetry()) These will be automatically reattempted on the next update cycle. @else
These will not be automatically updated until you clear the failed status within {{ PANEL_BRAND }}. To clear the status for each failed application follow these steps:

* Login to [{{PANEL_BRAND}}]({{ \Auth_Redirect::getPreferredUri() }})
* Go to **Web** > **Web Apps**
* Find the failed application, click **Select**
* Under _App Meta_ > _Version_, click **Update**
* Upon successful update, the failed flag will be automatically reset
@endif

## Permission failures
If an update fails due to permissions, you can easily reset it to the owner:

* Login to [{{PANEL_BRAND}}]({{ \Auth_Redirect::getPreferredUri() }})
* Go to **Web** > **Web Apps**
* Find the failed application, click **Select**
* Under _Change Fortification_, click **Reset Permissions**
* Permissions will be reset to user specified under _Options_ > _User Ownership_

@endif

@include('email.webapps.common-footer')
@endcomponent