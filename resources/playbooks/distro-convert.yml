# Convert Red Hat-like OS into another variant
---
- hosts: localhost
  module_defaults:
    yum:
      lock_timeout: "{{ yum_lock_max_wait }}"
  become: yes
  connection: local
  gather_facts: yes
  vars_files:
    - 'apnscp-vars.yml'
    - 'roles/common/vars/apnscp-internals.yml'
    - ['{{ apnscp_user_defaults }}', '/dev/null']
    - ['{{ apnscp_last_run_vars }}', '/dev/null']
  vars:
    distro_pathways:
      # Add special condition for Stream <-> non-Stream
      CentOS:
        - AlmaLinux
        - Rocky
      AlmaLinux:
        - Rocky
      Rocky:
        - AlmaLinux
    distro_scripts:
      AlmaLinux: "{{ apnscp_root }}/resources/storehouse/distro/alma.sh"
      Rocky: "{{ apnscp_root }}/resources/storehouse/distro/rocky.sh -r"
    logfiles:
      # almalinux-deploy.sh line 42
      AlmaLinux: /var/log/almalinux-deploy.log
      # migrate2rocky.sh line 56
      Rocky: /var/log/migrate2rocky.log
  tasks:
    - name: Assert version is sufficient
      assert:
        that: ansible_distribution_major_version == '8'
        fail_msg: "Distro conversion is only available on CentOS 8+"
    - name: Assert target distro set
      assert:
        that: distro is defined
        fail_msg: "Missing distro variable"
    - name: Assert migration pathway exists
      assert:
        that: distro in distro_pathways[ansible_distribution]
        fail_msg: "Distro target cannot be migrated from {{ ansible_distribution }}"
    - name: Convert CentOS Stream to CentOS 8
      block:
        - name: Remove centos-release-stream
          shell:
            rpm -e --nodeps centos-stream-release centos-stream-repos
          args:
            warn: False
        # dnf as invoked by Ansible blows up at this point, call rpm directly
        - name: Install centos-release
          shell: >-
            rpm -i --nodeps http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/centos-linux-repos-8-2.el8.noarch.rpm ;
            rpm -i http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/centos-linux-release-8.4-1.2105.el8.noarch.rpm
          args:
            warn: False
      when:
        - distro == 'AlmaLinux'
        - ansible_distribution_major_version == ansible_distribution_version
        - ansible_distribution == 'CentOS'
    - name: distro-sync dependency precheck
      shell:
        dnf distro-sync
      args:
        warn: False
      environment:
        LANGUAGE: en_US
      register: o
      changed_when: -1 != o.stdout.find("Nothing to do.")
      failed_when: 1 == o.rc and -1 != o.stdout.find("Error")
    - debug: msg="Running update script. Log is located in {{ logfiles[distro] }}"
      when: distro in logfiles
    - name: Run conversion script {{ ansible_distribution }} => {{ distro }}
      shell:
        "{{ distro_scripts[distro] }}"
    - name: Update ansible_distribution
      include_role:
        name: packages/install
      vars:
        ansible_distribution: "{{ distro }}"
