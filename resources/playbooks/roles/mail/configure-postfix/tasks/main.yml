---
# When changed this often suggests FST Postfix is desync'd from root filesystem
- name: Populate FST defaults
  include_tasks: set-configuration.yml
  vars:
    config_file: "{{ apnscp_filesystem_template }}/siteinfo/{{ postfix_config_file }}"
    config: "{{ postfix_fst_config }}"
    notify: Reload filesystem template

- name: Setup cyrus
  include_tasks: setup-cyrus.yml

- name: Import haproxy vars
  include_vars: "{{ playbook_dir }}/roles/software/haproxy/defaults/main.yml"
  when: haproxy_enabled | bool and not (software_haproxy_marker is defined)

- name: Verify host is set if smarthost enabled
  fail: msg="postfix_smarthost_enabled is true and postfix_smarthost_credentials.host is not set"
  when:
    - (postfix_smarthost_enabled | bool)
    - not postfix_smarthost_credentials.host
- name: Configure smarthost support
  include_tasks: configure-smarthost.yml
  vars:
    credentials: "{{ postfix_smarthost_credentials }}"
  when: postfix_smarthost_enabled | bool
- fail:
    msg: >-
      NON-FATAL NOTICE: Data center mode enabled but address verify map or
      postscreen map not shared in memcache instance! Set postfix_address_verify_map
      and postfix_postscreen_cache_map for best performance.
  ignore_errors: true
  when: >
    data_center_mode and (
      -1 == postfix_address_verify_map.find("memcache:") or
      -1 == postfix_postscreen_cache_map.find("memcache:")
    )

- name: Update master.cf
  template:
    src: "{{ postfix_master_template }}"
    dest: /etc/postfix/master.cf
  when: (postfix_master_template | string) | length
  notify: Restart postfix

- include_vars: "{{playbook_dir}}/roles/mail/maildir/defaults/main.yml"
- include_tasks: register-password.yml

- include_tasks: install-postsrs.yml
  when: postsrsd_enabled | bool

- name: Remove postsrsd
  yum: name=postsrsd state=absent
  when: not postsrsd_enabled | bool
- name: Set original sender in X-Envelope-From header
  lineinfile:
    path: '{{ access_file }}'
    line: '{{ postfix_envelope_save_header_check }}'
    regexp: ' PREPEND\s+{{ postfix_envelope_save_header_name | regex_escape() }}:'
    state: present
  notify: Update Postfix access

- name: Create Postfix user
  postgresql_user:
    name: "{{ postfix_user }}"
    password: "{{ postfix_password }}"
    db: "{{ postfix_db }}"
    encrypted: YES
    priv: "uids:select/gids:select/domain_lookup:select/email_lookup:select"
    login_user: root
  register: user
  no_log: True

- name: Merge custom config
  include_tasks: "{{ playbook_dir }}/roles/common/tasks/implicitly-import-overrides.yml"
  vars:
    base: "{{ postfix_config }}"
    varname: postfix_config
    prefix: postfix
    name: ""

- name: Copy Postfix configuration templates
  template:
    src: "templates/{{ item }}"
    dest: "{{ postfix_conf_dir }}/{{ item | regex_replace('\\.j2$', '') }}"
    owner: root
    group: "{{ postfix_sysuser }}"
    mode: 0640
    force: no
  with_items: "{{ template_files }}"
  notify: Restart postfix

- name: Set main.cf configuration
  include_tasks: set-configuration.yml
  vars:
    config: "{{ postfix_config }}"
    config_file: "{{ postfix_config_file }}"
- name: Create aliases
  include_tasks: manage-alias.yml
  vars:
    email: "{{ item.key }}"
    destination: "{{ item.value }}"
  loop: "{{ postfix_aliases | dict2items }}"
  loop_control:
    label: "Adding alias {{ item.key }} to {{ item.value }}"

- name: Check if sender transport creation necessary
  stat: path="{{ postfix_conf_dir}}/sender_transport"
  register: st
- name: Create sender transport
  block:
  - file:
      path: "{{ postfix_conf_dir}}/sender_transport"
      state: touch
  - command: /usr/sbin/postmap "{{ postfix_conf_dir }}/sender_transport"
  when: not st.stat.exists
- name: Update transport
  command: /usr/sbin/postmap "{{ postfix_conf_dir }}/transport"
  changed_when: false

  # Postfix looks for maps in main.cf, not backups specified
- name: Populate backup maps
  include_tasks: create-map.yml
  vars:
    path: "{{ item | regex_replace('^[^/]*', '')}}"
    type: "btree"
    suffix: "db"
  with_items:
    - "{{ postfix_local_address_verify_map }}"
    - "{{ postfix_local_postscreen_cache_map }}"
  when: (postfix_memcache_server | default('', true) | length > 0)

- name: Bug #30599 workaround
  include_role: name=common tasks_from=ini-bug-30599.yml
  vars:
    path: "{{ postfix_conf_dir }}/{{ item.config }}"
    key: "key_format"
  with_items:
    - "{{ postfix_memcache_maps }}"
  when: ((postfix_memcache_server | default('', true)) | length > 0)

- name: Set postscreen/address verify caches
  include_tasks: set-configuration.yml
  vars:
    config_file: "{{ postfix_conf_dir }}/{{ subitem.config }}"
    config: "{{ subitem.params }}"
    notify: Restart postfix
  when: ((postfix_memcache_server | default('', true)) | length > 0)
  loop_control:
    label: "Setting {{ subitem.config }} config"
    loop_var: subitem
  with_items: "{{ postfix_memcache_maps }}"
