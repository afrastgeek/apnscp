---
- name: Create cgroup in FST
  file:
    path: "{{ apnscp_filesystem_template }}/{{cgroup_service}}/{{cgroup_path | dirname }}"
    state: directory
- name: Link cgroup to {{ apnscp_shared_root }}
  file:
    path: "{{ apnscp_filesystem_template }}/{{cgroup_service}}/{{cgroup_path }}"
    src: "{{ apnscp_shared_root }}/cgroup"
    state: link
    force: yes
- name: Copy cgroup structure
  synchronize:
    src: "{{ cgroup_path }}/"
    archive: yes
    recursive: no
    links: yes
    times: no
    rsync_opts:
      - '--keep-dirlinks'
      - '--dirs'
    dest: "{{ apnscp_shared_root }}/cgroup/"
- name: Create cgrules.conf skeleton
  copy:
    dest: "{{ apnscp_filesystem_template }}/{{ cgroup_service }}/etc/cgrules.conf"
    content: ""
  notify: Reload filesystem template
- name: Set cgroup configuration
  ini_file:
    path: "{{ apnscp_root }}/config/custom/config.ini"
    section: cgroup
    option: "{{ item.option }}"
    value: "{{ item.value }}"
  with_items:
    - option: home
      value: "{{ cgroup_path }}"
    - option: controllers
      value: "{{ cgroup_controllers|join(',') }}"
- name: Set cgroup configuration in /etc/mtab
  lineinfile:
    path: "{{ apnscp_filesystem_template }}/siteinfo/etc/mtab"
    regexp: '^cgroup\s+{{ cgroup_path }}/{{item}}\s+'
    line: 'cgroup {{ cgroup_path }}/{{item}} cgroup rw,nosuid,nodev,noexec,relatime,{{item}} 0 0'
    state: present
  with_items: "{{ cgroup_controllers }}"
  notify: Reload filesystem template
- name: Set Delegate=yes in cgconfig service
  include_role: name=systemd/override-config
  vars:
    service: cgconfig
    config:
      - group: Service
        vars:
          Delegate: "yes"
- name: Enable cgconfig
  systemd:
    name: cgconfig
    state: started
    enabled: yes

- name: Enable cgroupv2
  replace:
    path: /etc/default/grub
    # Crap assertion that GRUB_CMDLINE_LINUX has directives before it. Python has gnarly atomic grouping, so for maintainability
    # let's assume
    regexp: '^GRUB_CMDLINE_LINUX\s*=([''"])?((?:.(?!\s*systemd\.unified_cgroup_hierarchy=))*.)(?:\s*systemd\.unified_cgroup_hierarchy=[10])?(.*)\1$'
    replace: 'GRUB_CMDLINE_LINUX=\1\2 systemd.unified_cgroup_hierarchy={{ (cgroup_unified_hierarchy | bool) | ternary("1", "0") }}\3\1'
  when:
    - ansible_distribution_major_version == "8"
    - '"unified_cgroup_hierarchy" in lookup("file", "/etc/default/grub") or (cgroup_unified_hierarchy | bool) == True'
  register: r
  notify: Update grub
- block:
  - name: Save argv
    get_argv:
  - include_role: name=common/fail-and-save
    vars:
      reboot: true
      msg: |
        Bringing up kernel {% if cgroup_unified_hierarchy %} with {% else %} without {% endif %} unified cgroups!
  when: r.changed
