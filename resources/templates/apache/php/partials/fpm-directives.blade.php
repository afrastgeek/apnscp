SetEnv PHP_HANDLER "proxy:unix:{{ $fpmConfig->getSocketPath() }}|fcgi://localhost"

<IfModule mod_php5.c>
	php_admin_flag engine off
</IfModule>

<IfModule mod_php7.c>
	php_admin_flag engine off
</IfModule>

<Files "*.php">
	<If "-f %{SCRIPT_FILENAME}">
		{{-- don't evaluate wp-content/uploads/, cache/, or Drupal sites/ files --}}
		<If "! %{SCRIPT_FILENAME} =~ m#^{{ rtrim($fpmConfig->getRoot(), '/') }}/(?:var/[^/]+|home/[^/]+)/(?:[^/]+/)+(?:wp-content/uploads/|cache/|sites/default/).*$#">
			{{-- send jailed DOCUMENT_ROOT path --}}
			ProxyFCGISetEnvIf "reqenv('VPATH') =~ m|^{{ rtrim($fpmConfig->getRoot(), '/') }}(/.++)$|" DOCUMENT_ROOT "$1"
			{{-- SCRIPT_NAME = "" | ( "/" path ) per RFC 3875 --}}
			ProxyFCGISetEnvIf "true" SCRIPT_FILENAME "%{reqenv:DOCUMENT_ROOT}%{reqenv:SCRIPT_NAME}"
			{{-- systemd translates to the mount the file is on, siteXX/fst/ in this case --}}
			SetHandler "proxy:unix:{{ $fpmConfig->getSocketPath() }}|fcgi://localhost"
		</If>
	</If>
</Files>

<Proxy "fcgi://localhost" max={{ $fpmConfig->getProxyThreadsPerServer() }} acquire={{ $fpmConfig->getProxyConnectionTimeout() }}>
	@includeIf('php.partials.proxy-settings')
</Proxy>
