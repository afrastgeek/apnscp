{{--
	All records must not contain any indention. Validate the template with:
	cpcmd dns:validate-template TEMPLATE_NAME

	Note:
		- dns:validate-template respects provider-specific RR capabilities.
		- host records must include trailing period (foo.bar.com.)
		- IN class is required, but HS and CH may also be used
		- \Regex::DNS_AXFR_REC_DOMAIN is used for validation
		- $ips refers to public web server IPs
--}}
@foreach($ips as $ip)
@php $rr = false === strpos($ip, ':') ? 'A' : 'AAAA'; @endphp
@foreach(['','www.','ftp.'] as $sub)
{!! $sub !!}{!! $hostname !!}. {!! $ttl !!} IN {!! $rr !!} {!! $ip !!}
@endforeach
@endforeach
@if (!$subdomain && ($uuid = $svc->getSiteFunctionInterceptor()->dns_uuid()))
{!! $svc->getSiteFunctionInterceptor()->dns_uuid_name() !!}.{!! $zone !!}. {!! $ttl !!} IN TXT "{!! $uuid !!}"
@endif
