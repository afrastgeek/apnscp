RewriteEngine on
RewriteRule ^(.*)$ https://%{ENV:HOSTNAME}/{{ $mailpath }}/$1 [R,L]