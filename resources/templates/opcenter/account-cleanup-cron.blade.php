#!/bin/sh

TIMESPEC="{{ $timespec }}"
# To ensure at no point would a scheduled panel restart interrupt deletion
sleep 3600
exec "{{ $apnscp_root }}/bin/DeleteDomain" --since="$TIMESPEC"