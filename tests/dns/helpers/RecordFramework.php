<?php declare(strict_types=1);

class RecordFramework extends TestFramework {
	use apnscpFunctionInterceptorTrait {
		setApnscpFunctionInterceptor as setApnscpFunctionInterceptorProxied;
	}
	// @var array simple pair of RR, before => after transformations
	const RECORDS = [
		'A' => [
			'1.2.3.4',
			'1.2.3.5'
		],
		'AAAA' => [
			'::1',
			'::2'
		],
		'CAA' => [
			'0 issue letsencrypt.org.',
			'0 issuewild comodoca.com'
		],
		'CNAME' => [
			'foo.bar.com',
			'qux.bar.com'
		],
		'DNAME' => [
			'foo.abc.com',
			'qux.aa.czz'
		],
		'HINFO' => [
			'"INTEL-386" "Windows"',
			'"AMD-686" "Linux"'
		],
		'NS' => [
			'ns1.apisnetworks.com',
			'ns2.apisnetworks.com'
		],
		'MX' => [
			'10 mail.apisnetworks.com.',
			'0 mail.apisnetworks.com.'
		],
		'RP' => [
			'root.trantor.umd.edu. ops.CS.UMD.EDU.',
			'gregh.sunset.umd.edu.  .'
		],
		'TXT' => [
			'v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA03P11GTQfj0oY6JNuUTygLVt7nGlbWP+XYZ//XSD/lm7fss3i0H4uJR+XZRf9w5/x67ic4ClM7dc3LX8BsQRRBweUmt/AtMLGX39bLlpWEburpNnx75rnhEChH/Cl8Msot3tlExMRDqvyvJha7pHhGzoq5ePhFBrhc4UKB1tTjYZVDpZmjt/I6NStOMw6azQidGYcYTQlY7M+ZSSY8Z9HsK/8O+ZVBNO1uf+HisMaMFpeu276I5uZhzkGMb1HF6fStuUwILnn5ZV7G2CXTk9Vdj7oNXUsHTwP3tAHoE3hZqovL9QBncu0MnLf9Y9yKQIDYAB',
			'v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA03P11GTQfj0oY6JNuUTygLVt7nGlbWP+XYZ//XSD/lm7fss3i0H4uJR+XZRf9w5/x67ic4ClM7dc3LX8BsQRRBweUmt/AtMLGX39bLlpWEburpNnx75rnhEChH/Cl8Msot3tlExMRDqvyvJha7pHhGzoq5ePhFBrhc4UKB1tTjYZVDpZmjt/I6NStOMw6azQidGYcYTQlY7M+ZSSY8Z9HsK/8ONEWLONGVERSIONHisMaMFpeu276I5uZhzkGMb1HF6fStuUwILnn5ZV7G2CXTk9Vdj7oNXUsHTwP3tAHoE3hZqovL9QBncu0MnLf9Y9yKQIDYAB'
		],
		'LOC' => [
			'37 46 46 N 122 23 35 W 1m 100m 2m 3m',
			'12 46 46 N 122 23 35 E 1m 122m 3m 4m'
		],
		'PTR' => [
			'subdomain' => '1.2.3.4',
			'old'       => 'bar.com',
			'new'       => 'baz.com'
		],
		'SRV' => [
			'subdomain' => '_sip._tcp',
			'old'       => '0 0 535 mail.google.com',
			'new'       => '10 0 587 mail.google.com'
		],
		'SPF' => [
			'v=spf1 a ip:198.51.100.4',
			'v=spf1 a ip:198.51.100.4 -all'
		],
		// @TODO fails if provider begins strict checking
		'CERT' => [
			'9 1 8 dGVzdAo=',
			'10 1 0 dGVzdAo='
		],
		'DNSKEY' => [
			'1 3 5 dGVzdAo=',
			'2 4 6 dGVzdAo='
		],
		'DS' => [
			'1 3 1 4E1243BD22C66E76C2BA9EDDC1F91394E57F9F83',
			'2 5 2 4E1243BD22C66E76C2BA9EDDC1F91394E57F9F83'
		],
		'NAPTR' => [
			'100 10 "U" "E2U+sip" "!^.*$!sip:customer-service@example.com!" .',
			'200 50 "U" "E2U+sip" "!^.*$!sip:customer-service@somewhere.else!" .'
		],
		'SMIMEA' => [
			'3 0 0 d2abde240d7cd3ee6b4b28c54df034b9',
			'9 9 9 7983a1d16e8a410e4561cb106618e971'
		],
		'SOA'   => [
			'subdomain' => '',
			'old'       => null,
			'new'       => null
		],
		'SSHFP' => [
			'1 1 4E1243BD22C66E76C2BA9EDDC1F91394E57F9F83',
			'1 2 F2CA1BB6C7E907D06DAFE4687E579FCE76B37E4E93B7605022DA52E6CCC26FD2'
		],
		'SVCB' => [
			'1 . alpn="h3,h3-29,h3-28,h3-27,h2" ipv4hint=104.18.26.46,104.18.27.46 ipv6hint=2606:4700::6812:1a2e,2606:4700::6812:1b2e',
			'1 . alpn="h3,h2" ipv4hint=105.18.26.46,104.18.27.46 ipv6hint=2106:4700::6812:1a2e,2606:4700::6812:1b2e'
		],
		'TLSA' => [
			'0 0 1 b5bb9d8014a0f9b1d61e21e796d78dccdf1352f23cd32812f4850b878ae4944c',
			'1 1 2 cc06808cbbee0510331aa97974132e8dc296aeb795be229d064bae784b0a87a5cf4281d82e8c99271b75db2148f08a026c1a60ed9cabdb8cac6d24242dac4063'
		],
		'URI' => [
			'10 10 http://example.com/foo',
			'10 10 https://panel.dev'
		]
	];
	protected $context;
	protected $afi;

	// @var array records to test
	protected $records;

	// @var string provider tag
	protected $providerTag;
	// @var string DNS provider
	protected string $provider;

	public function __construct(Auth_Info_User $context, string $provider)
	{
		$this->context = $context;
		$this->provider = $provider;
		$this->setModuleAuth($context, $provider);

		$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($context));
		$this->assertTrue(\Opcenter\Dns::providerValid($provider), "Named provider exists");
		$this->assertNotFalse($this->dns_get_provider(), 'DNS provider API method exists');
		$this->swapProvider($provider);
		$this->assertSame($provider, $this->dns_get_provider(), 'Provider configuration set');
		if (!$this->getApnscpFunctionInterceptor()->dns_zone_exists($this->context->domain)) {
			$module = \Dns_Module::instantiateContexted($this->context)->_proxy();
			$module->add_zone($this->context->domain, $this->getApnscpFunctionInterceptor()->common_get_ip_address()[0]);
		}
		$this->assertTrue($this->getApnscpFunctionInterceptor()->dns_zone_exists($this->context->domain),
			'Verify zone ' . $this->context->domain . ' exists (' . $this->getApnscpFunctionInterceptor()->dns_get_provider()  . ')');
		$this->providerTag = '[' . $provider . ']';
	}

	public function setApnscpFunctionInterceptor(apnscpFunctionInterceptor $afi): void
	{
		$this->setApnscpFunctionInterceptorProxied($afi);
		$this->swapProvider($this->provider);
	}

	private function swapProvider(string $driver): void
	{
		$swapped = \Module\Provider::get('dns', $driver, $this->context);
		$this->assertContains($driver, strtolower(get_class($swapped)), 'Module loaded');
		$this->getApnscpFunctionInterceptor()->swap('dns', $swapped);
	}

	private function setModuleAuth(Auth_Info_User $context, string $driver) {
		$auth = array_get(Definitions::get(), "dns.${driver}.key", []);

		$context->conf->changeMulti(['dns' => [
				'provider' => $driver,
				'key' => $auth
			]
		]);
	}

	public function getRecords(): array
	{
		return $this->getApnscpFunctionInterceptor()->dns_permitted_records();
	}

	public function testRecord(string $rr)
	{
		if ($rr === 'PTR') {
			return;
		}

		$recordData = static::RECORDS[$rr];
		$subdomain = \Opcenter\Auth\Password::generate(8, 'a-z');
		if ($rr === 'SOA' || $rr === 'DNSKEY') {
			$subdomain = '';
		}
		$old = $new = '';
		if (isset($recordData[0])) {
			[$old, $new] = $recordData;
		} else {
			foreach ($recordData as $k => $v) {
				${$k} = $v;
			}
		}
		if (null === $old) {
			$old = $this->dns_get_records_by_rr($rr, $this->context->domain)[0]['parameter'];
		}

		$ttl = random_int(600, 86400);
		if ($rr === 'DS') {
			$this->assertTrue($this->attempt('add_record', array_merge(compact('subdomain', 'rr', 'old', 'ttl'), ['rr' => 'NS', 'old' => self::RECORDS['NS'][0]])),
				"DS requires NS record " . $this->providerTag);
		}

		if ($rr === 'SOA') {
			$dnsModule = $this->getApnscpFunctionInterceptor()->get_loaded_modules()->getModule('dns');
			$record = $dnsModule::createRecord($this->context->domain,
				['name' => $subdomain, 'rr' => $rr, 'parameter' => $old]);
			$record->setMeta('serial', (int)$record->getMeta('serial') + 1);
			$new = $record->parameterFromMeta();
		}

		$this->tryAdd(compact(['rr', 'subdomain', 'old', 'ttl']));

		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
		$this->assertTrue($this->attempt('modify_record', compact('subdomain', 'rr', 'old') + [99999 => ['parameter' => $new]]));
		\Error_Reporter::exception_upgrade($oldex);
		$ttl = random_int(600, 86400);
		for ($i = 0; $i < 10; $i++) {
			if ($this->attempt('record_exists', compact('subdomain', 'rr', 'new', 'ttl'))) {
				break;
			}
			sleep(1);
		}
		if ($i === 10) {
			warn('Possible asynchronous record detected. ' .
				"Remote record query failed using dig on `%s': `%s', continuing", $rr, $new
			);
		}

		if ($rr === 'SOA') {
			// PowerDNS always grows this value on update. Let's emulate so the test completes.
			$new = $record->setMeta('serial', (int)$record->getMeta('serial') + 1)->parameterFromMeta();
			$record->setMeta('serial', (int)$record->getMeta('serial') + 1);
			// icky. Reset the record to its former value. For use with SOA only.
			$this->assertTrue($this->attempt('modify_record', compact('subdomain', 'rr', 'new') + ['foo' => ['parameter' => $record->parameterFromMeta()]]));

		} else {
			$this->tryRemove(compact(['rr', 'subdomain', 'new']));
		}
	}

	private function tryAdd($args) {
		extract($args, EXTR_OVERWRITE);
		if ($rr === 'SOA') {
			return;
		}

		$this->assertTrue($this->attempt('add_record', compact('subdomain', 'rr', 'old', 'ttl')),
			"Add ${rr} record " . $this->providerTag);
		for ($i = 0; $i < 10; $i++) {
			if ($this->attempt('record_exists', compact('subdomain', 'rr', 'old'))) {
				break;
			}
			sleep(1);
		}
		if ($i === 10) {
			warn('Possible asynchronous record detected. ' .
				"Remote record query failed using dig on `%s': `%s', querying API for result", $rr, $old
			);
		}
	}

	private function tryRemove($args) {
		extract($args, EXTR_OVERWRITE);

		if ($rr === 'SOA') {
			return;
		}

		if ($rr === 'DS') {
			$this->assertTrue($this->attempt('remove_record', array_merge(compact('subdomain', 'rr'), ['rr' => 'NS'])),
				"Remove NS record " . $this->providerTag);
		}
		$this->assertTrue($this->attempt('remove_record', compact('subdomain', 'rr', 'new')),
			"Remove ${rr} record " . $this->providerTag);
	}

	public function testWeakMatchRemoval() {
		$subdomain = strtolower(\Opcenter\Auth\Password::generate(16));
		$this->assertTrue(
			$this->attempt('add_record', [$subdomain, 'TXT', 'testing']),
			"Add weak test record " . $this->providerTag
		);

		$this->assertTrue(
			$this->attempt('remove_record', [$subdomain, 'TXT', '']),
			'Record removed without specifying parameter ' . $this->providerTag
		);
	}

	public function testNonSpecificCheck() {
		if ($this->dns_get_provider() === 'null') {
			$this->markTestSkipped('null always returns false');
		}
		if ($this->dns_get_provider() === 'katapult') {
			$this->markTestSkipped('DNS non-authoritative until confirmed');
		}
		$subdomain = strtolower(\Opcenter\Auth\Password::generate(16));
		$this->assertFalse(
			$this->attempt('record_exists', [$subdomain, 'MX']),
			"Check non-existent record " . $this->providerTag
		);

		$this->assertTrue(
			$this->attempt('add_record', [$subdomain, 'MX', '5 example.com.']),
			"Add MX record on hostname"
		);

		$res = false;
		// coerce cache refresh
		$this->dns_get_records_by_rr('MX', $this->context->domain, $subdomain);
		for ($i = 0; $i < 120; $i++) {
			$res = $this->attempt('record_exists', [$subdomain, 'MX']);
			if ($res) {
				break;
			}
			sleep(1);
		}
		$this->assertTrue($res, "Confirm existent record, non-specific MX " . $this->providerTag);
	}

	public function testReset() {
		$this->assertTrue(
			serial(function () {
				$page = \Page_Container::init(\apps\dns\Page::class);
				$params = [
					'domain'  => $this->context->domain,
					'toolbox' => 'restore'
				];
				$page->on_postback($params);
				return true;
			}), 'DNS reset succeeded ' . $this->providerTag
		);

	}

	public function testAnyRecord() {
		$subdomain = strtolower(\Opcenter\Auth\Password::generate(16));
		$this->assertFalse(
			$this->attempt('record_exists', [$subdomain, 'ANY']),
			"ANY record fail " . $this->providerTag
		);

		$this->assertTrue(
			$this->attempt('record_exists', [$subdomain, 'A', '1.2.3.4']),
			"Create record for ANY test " . $this->providerTag
		);

		$this->assertTrue(
			$this->attempt('record_exists', [$subdomain, 'ANY']),
			"ANY record success " . $this->providerTag
		);
	}

	private function attempt(string $method, array $args): bool
	{
		$fn = "dns_${method}";
		return $this->$fn(
			$this->context->domain,
			...array_values($args)
		);
	}

}