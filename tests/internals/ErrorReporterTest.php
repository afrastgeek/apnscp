<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */
require_once dirname(__DIR__, 1) . '/TestFramework.php';

class ErrorReporterTest extends TestFramework
{
	public function testSilence() {
		$this->assertEmpty(\Error_Reporter::get_buffer());
		silence(function () {
			error("foo!");
		});
		$this->assertEmpty(\Error_Reporter::get_buffer());
	}
}

