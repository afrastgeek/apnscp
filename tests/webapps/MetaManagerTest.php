<?php

	use Module\Support\Webapps\Traits\PublicRelocatable;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class MetaManagerTest extends TestFramework
    {
        protected $filename;

        public function testWordpress()
        {
            $auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
            $afi1 = apnscpFunctionInterceptor::factory($auth1);

            $subdomain1 = "test2-" . uniqid();
            $this->assertTrue($afi1->web_add_subdomain($subdomain1, '/var/www/' . $subdomain1));
            $this->assertTrue($afi1->wordpress_install($subdomain1, '', [
                'version' => array_get(Definitions::get(), 'webapps.install.wordpress.version', '4.9.11'),
                'ssl' => false,
                'notify' => false
            ]));
            $this->assertEquals(
                $afi1->wordpress_get_version($subdomain1),
                array_get(Definitions::get(), 'webapps.install.wordpress.version', '4.9.11')
            );
            $this->assertTrue($afi1->wordpress_uninstall($subdomain1));
            $this->assertTrue($afi1->web_remove_subdomain($subdomain1));
            $afi1->file_delete('/var/www/' . $subdomain1, true);
        }

        public function testMetaRelocation() {
        	$class = new class extends \Module\Support\Webapps {
				use PublicRelocatable {
					getAppRoot as getAppRootReal;
				}

				public function install(string $hostname, string $path = '', array $opts = array()): bool
				{

					if (null === ($docroot = $this->remapPublic($hostname, $path))) {
						// it's more reasonable to fail at this stage, but let's try to complete
						return error("Failed to remap");
					}

					$approot = $this->getAppRoot($hostname, $path);
					// @todo migrate cache management to reconfigure method
					$this->initializeMeta($docroot, ['foo' => 'bar']);
					return true;
				}

				public function get_versions(): array
				{
					return ['1.0'];
				}

				public function get_version(string $hostname, string $path = ''): ?string
				{
					return '1.0';
				}

				public function valid(string $hostname, string $path = ''): bool
				{
					return true;
				}

				public function getModule(): string
				{
					return 'unknown';
				}

				public function getAppName(): ?string
				{
					return 'unknown';
				}
			};


			$account = \Opcenter\Account\Ephemeral::create();
			$auth = $account->getContext();
			$afi = $account->getApnscpFunctionInterceptor();
			$prefs = \Preferences::factory($auth)->unlock($afi);
			\Auth::set_handler(\Auth::autoload()->setID($auth->id));
			$this->assertSame($auth->id, \Auth::profile()->id);
			$class = $class::instantiateContexted($auth);
			$this->assertCount(0, array_get($prefs, \Module\Support\Webapps::APPLICATION_PREF_KEY, []), 'Pre-install pref assertion');
			$this->assertTrue($class->install($auth->domain));
			$this->assertCount(1, array_get($prefs, \Module\Support\Webapps::APPLICATION_PREF_KEY, []),
				'Post-install pref assertion');
			$this->assertSame(array_get($prefs, \Module\Support\Webapps::APPLICATION_PREF_KEY), \Preferences::get(\Module\Support\Webapps::APPLICATION_PREF_KEY), 'Global state matches instantiated state');
			$this->assertArrayHasKey('/var/www/html-undefined/public', array_get($prefs, \Module\Support\Webapps::APPLICATION_PREF_KEY, []), 'Docroot renamed');
			$this->assertTrue($class->uninstall($auth->domain));
			$this->assertArrayHasKey(Web_Module::MAIN_DOC_ROOT, array_get($prefs, \Module\Support\Webapps::APPLICATION_PREF_KEY, []));
			$this->assertCount(1, \Preferences::get(\Module\Support\Webapps::APPLICATION_PREF_KEY, []));


		}
    }

