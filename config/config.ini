;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   ApisCP master configuration   ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ************ WARNING ************
; DO NOT EDIT DIRECTLY.
; SET NEW VALUES IN conf/custom/config.ini
; OR USE CLI HELPER:
; cpcmd scope:set cp.config <section> <name> <value>
; ************ WARNING ************
;
; Modifying any value requires a panel restart:
;	systemctl restart apiscp
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;
;;; Core configuration that affects all aspects of ApisCP
;;;
[core]
; Use env DEBUG=1 environment variable to trigger debug
debug = ${DEBUG}
; Display backtraces on (1) error, (2) warning, (3) info, (4) debug/deprecated
; all higher numbers imply lower class reporting; 4 produces backtrace on all
; backtrace occurs when debug set to true.
; Set to -1 to disable backtrace on ApisCP-generated events,
; but continue to display PHP error/warning/notice backtraces
debug_backtrace_qualifier=1

; PROTECTED
; Global temp directory, reflected within virtual domains
temp_dir = /tmp

; In multiserver setups behind a proxy (cp-proxy),
; trust the following source IP or network for X-Forwarded-For
; See https://github.com/apisnetworks/cp-proxy
; One or more addresses may be listed each separated by a comma
http_trusted_forward =
; PROTECTED
; Root directory that stores all
filesystem_virtbase = /home/virtual
; PROTECTED
; Filesystem template location
filesystem_template = /home/virtual/FILESYSTEMTEMPLATE

; PROTECTED
; A path that is shared across all sites as read/write
filesystem_shared = /.socket

; PROTECTED
; Location for run files
run_dir = storage/run

; Force locale setting. Leave blank for system default
locale =
; Force timezone setting. Leave blank for system default, overrides php.ini
timezone =
; Send a copy of all unhandled errors generated in ApisCP
bug_report =

; Brand name for the panel, for white-label
panel_brand="ApisCP"
; PROTECTED
; ApisCP version
apnscp_version="3.1"
; PROTECTED
; ApisCP system user
apnscp_system_user=nobody
; preload backend modules
; increases backend initialization but checks for errors
fast_init=!${DEVELOPMENT}

[frontend]
; Frontend PHP dispatcher, embed or fpm (dev only!)
dispatch_handler=embed
; Only expose https ports on public IPs (ports :2083 + :2078).
; 127.0.0.1:2082 will still be bound for internal usage.
https_only=false
; Content security policy to apply. Leave blank to disable.
; A good default would be:
; "default-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline'; img-src 'self' data: secure.gravatar.com;"
content_security_policy =
; Rewrite all external links to open in a new tab.
; "ui-action-external" style is added as :after pseudoelement
; - "_blank" may be specified to create a new link in a new tab each time.
; - NAME may be specified to refer to a specific tab name
; Default is "_new" when "true"
external_opener=false
; Certain locations are protected using a challenge cookie set for authenticated sessions
; A request must present this cookie to be allowed access.
seckey_ttl=259200

[style]
; Default apnscp theme
theme = "apnscp"
; Allow custom themes
; See https://github.com/apisnetworks/apnscp-bootstrap-sdk
allow_custom = false
; Override ApisCP JS
override_js = false
; Forbid the following themes
; Setting * forbades all themes but assigned theme. Must come first.
; Themes can be negated with a ! prefix. To blacklist all themes
; but [style] => theme + "dorodoro", set
; blacklist="*,!dorodoro"
blacklist=
; Enable Gravatar display. Leave empty to disable
; Possible values: 404, mp, identicon, monsterid, wavatar, retro, robohash, blank
gravatar=identicon
; Use separate "Administrator" checkbox in login UI or merge login to single field.
; "Administrator" only exists to disambiguate "domain" field requirement.
; Possible values:
; 	- true: show "Administrator" checkbox
;	- false: hide "Administrator" checkbox
;	- thin: use single login field
verbose_login = true

;;;
;;; SOAP API
;;;
[soap]
; Enable soap? Disabling also disables server-to-server migrations
enabled = true
; PROTECTED
; WSDL name, located under htdocs/html/
wsdl = "apnscp.wsdl"
; Use a single database for API keys across all servers
; Note: each server must have the proper authentication in db.yaml => api
; and this database must also house "domain_information", part of cp-collect
; See https://docs.apiscp.com/admin/Panel%20proxy for more information
unify_keys = false

;;;
;;; Backend
;;;
[apnscpd]
; PROTECTED
; Location for apnscpd backend socket
; specify an absolute path to store outside of ApisCP
socket = storage/run/apnscp.sock
; Maximum number of backend workers permitted
max_workers = 5
; Minimum number of idle backend workers
min_workers = 1
; Workers to spawn initially
start_workers = 0
; Max backlog per worker
max_backlog = 20
; Interval in seconds to prune idle workers
collection_interval = 30
; Make panel a headless installation, no front-end loaded
; Driven entirely by CLI
headless = false
; Designed to avoid memleaks in idle Apache processes.
; Periodically checks and kills all children whose RSS is greater (in MB).
; Only a high watermark is implemented, which sets the absolute limit.
http_high_watermark=200
; RLIMIT_RSS is an advisory limit to how much much the kernel
; should allocate to the process.
rlimit_rss=128

;;;
;;; ApisCP brute-force deterrent
;;;
[anvil]
; Max authentication attempts before all further authentication is rejected
limit = 20
; Duration to retain anvil statistics
ttl = 900
; Minimum number of permitted logins before anvil kicks in
min_attempts = 3
; Whitelist for Anvil attempts
; Accepts networks and single IP addresses, separate with a comma
whitelist = 127.0.0.1
; Rate-limit requests - prevent same-URL brute-force effort. Ignores static assets.
request_limit=15
; Rate-limit window in seconds
request_limit_window=1

;;;
;;; DAV
;;;
[dav]
; Enable DAV
enabled = true
; Allow non-DAV browser requests + interface
browser = true
; Allow DAV in Apache. All resources must be secured separately
apache = false

;;;
;;; Ticket system + system generated emails
;;;
[crm]
; send a small, MMS-suitable, message when a high
; priority ticket is opened or reopened to here
short_copy_admin =
; System email to dispatch internal issues such as
; certificate renewal failures or tickets
copy_admin = apnscp@${HOSTNAME}
; Address used to send emails
from_address = apnscp@${HOSTNAME}
; From name for above address
from_name = ApisCP
; No-reply used for password reset and login alerts
from_no_reply_address = apnscp@${HOSTNAME}
; Generalied reply-to address for ticket system
reply_address = apnscp+tickets@${HOSTNAME}

[session]
; Maximum duration an idle session is valid
ttl = 15 minutes

;;;
;;; Backend cache
;;;
[cache]
; In multi-server installations, use the following
; Redis server as an aggregate cache otherwise
; local Redis is used.
; Ensure configuration includes periodic RDB dump or AOF rewrite
super_global_host =
super_global_port =

; SG password. Super global, if defined, is reachable
; over network and thus open to abuse. See also
; https://packetstormsecurity.com/files/134200/Redis-Remote-Command-Execution.html
super_global_password =

; Local ApisCP cache. Socket only; never use TCP
; as it contains sensitive data
socket_perms = 0600

;;;
;;; Let's Encrypt SSL
;;;
[letsencrypt]
; When signing a certificate use LE staging server
debug=true
; PROTECTED
; X1/R3 X509 authority key identifier
; R3 expires in Sept 2021
keyid=A8:4A:6A:63:04:7D:DD:BA:E6:D1:39:B7:A6:45:65:EF:F3:A8:EC:A1,14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6
; PROTECTED
; X1 X509 staging authority key identifier
staging_keyid=C0:CC:03:46:B9:58:20:CC:5C:72:70:F3:E1:2E:CB:20:A6:F5:68:3A,C0:CC:03:46:B9:58:20:CC:5C:72:70:F3:E1:2E:CB:20:A6:F5:68:3A
; Perform a DNS check on each hostname to ensure it is reachable
; If any hostname fails the ACME challenge, e.g. DNS points elsewhere, renewal
; will fail. Keep this on unless you know what you're doing
verify_ip=true
; Failure to renew one hostname will force all hostnames in the certificate to fail renewal
; ApisCP will dispatch a secondary email informing of failure when [crm] => copy_admin is set.
strict_mode=false
; Include alternative form of requested certificate
; e.g. foo.com includes www.foo.com and www.foo.com includes foo.com
; This requires that verify_ip=true
alternative_form=false
; Additional hostnames to request SSL for
additional_certs=
; Day range a certificate may renew automatically. lookahead is max days to renew
; before expiry; lookbehind is min days to renew.
;
; A lower bracker (lookbehind) is necessary to ensure defunct domains
; are not continuously renewed - or attempted for renewal - against LE's servers.
; Set lookbehind to a large negative int (-999) to attempt to renew all defunct
; certificates.
; Set lookahead to a large positive int (999) to force reissue for all certificates.
; Default settings attempt renewal 10 times, once daily.
lookahead_days=10
lookbehind_days=0
; Send a notification email to [crm] => copy_admin on certificate renewal failure
notify_failure=true
; Use a single account manged by server admin (cpcmd common:get-email)
unify_registration=true
; Attempt SSL issuance upon site creation. DNS must be properly configured.
; Test with [letsencrypt] => debug enabled to ensure the process works first to avoid
; generating excessive errors with LE's ACME server.
;
; Alternatively, specify --bootstrap to AddDomain at creation.
; Setting ssl,enabled=0 negates this feature.
auto_bootstrap=false
; Attempt to request SSL certificates for all domains on an account up to n times
; Each attempt is delayed 12 hours to accommodate network changes.
bootstrap_attempts=3
; Disable certain challenge modes. Use a comma-delimited list for each
; Challenges will still appear in letsencrypt:challenges but skipped during acquisition
disabled_challenges=tls-alpn
; Maximum time to wait for record propagation for DNS checks
dns_validation_wait=30

;;;
;;; DNS + IP assignent
;;;
[dns]
; When adding IP-based sites, range from which IP addresses
; may be allocated. Supports comma-delimited and CIDR notation
allocation_cidr=
; Hosting nameservers sites should use when hosted through the panel
; Leave empty to disable a NS checks
hosting_ns=
; Vanity nameservers that when any of which are present satisfy the nameserver
; check for the domain. Unlike hosting_ns, which ALL must match any MAY match.
; When empty, "hosting_ns" will be used. When "hosting_ns" is empty this is ignored.
vanity_ns=
; Recursive nameservers used to verify visibility of DNS records
recursive_ns=127.0.0.1
; Nameserver that responds authoritatively for any account hosted. Used primarily for PTR
; assignments. This value is superceded by DNS providers *except for* PTR checks
authoritative_ns=
; A single internal master responsible for handling rndc/nsupdate and internal DNS queries
internal_master=
; Primary IP address of the server used in multi-homed environments, leave blank to autodiscover
my_ip4=
; Primary IPv6 address of the server used in multi-homed environments, leave blank to autodiscover
my_ip6=
; Default remote IPv4 address for DNS. See docs/NAT.md
proxy_ip4=
; Default remote IPv6 address for DNS. See docs/NAT.md
proxy_ip6=
; PROTECTED
; Use provider if dns,provider is not set for domain.
; Specifying "DEFAULT" as DNS provider will use this inherited configuration.
; Use "dns.default-provider" Scope to change this value.
provider_default="null"
; PROTECTED
; Optional global provider key, same form as dns,provider
; Use "dns.default-provider-key" Scope to change this value.
provider_key=
; UUID to assign this server. UUIDs are used to collaborate with different servers
; to determine whether to remove a DNS zone, e.g. moving server -> server with different
; UUIDs will persist the records when the domain is deleted from Server A so long as the DNS UUID
; differs
uuid=
; PROTECTED
; UUID record name for white-labeling. When changing UUID, in-place UUID records are NOT renamed.
; Changing this record may be an easy way to protect a cluster of domains from deletion,
; but so too is changing the uuid= value.
uuid_name=_apnscp_uuid
; Default TTL value for newly created DNS records. Time in seconds (4 hour default)
default_ttl=14400
; For NAT'd systems, when connecting to the public IPv4/IPv6
; determine if router is capable of looping back public => private IP
; -1 performs an autodetection, 0 router lacks capability, 1 router supports
hairpin=-1
; Maximum time to wait for zone propagation checks. Set to 0 to disable
; zone propagation wait checks.
validation_wait = 10
; Create a www record for subdomains in addition to the subdomain DNS record.
subdomain_implicit_www=true

[mail]
; SMTP/IMAP/POP3 uses proxied content handler
; Options are "haproxy" or "null"
proxy =
; Spam filter in use, "spamassassin" or "rspamd"
spam_filter=spamassassin
; Default provider to use for mail
; "builtin" relies on Postfix "null" for testing
provider_default=builtin
; Optional global provider key, same as dns,key
provider_key =
; Domain to masquerade as when sending mail
; Affects "Message-ID" generation + non-fully qualified addresses
sending_domain = "${HOSTNAME}"
; rspamd installed on server. Used for spam filtering + DKIM signing requests
rspamd_present=false
; Permit users to forward catch-alls elsewhere. This carries disastrous
; consequences, but may be useful for bulk imports from a source that allows such behaviors
forwarded_catchall = false
; Disallow all forms of forwarding off-server. Setting true implies forward_catchall=false
disabled_forwarding = false
; DKIM signing mode. Requires rspamd in outbound scanning mode.
; Possible values: true, false
dkim_signing = false
; Default SPF record added for newly created domains. If DKIM is not used, convert ~ to -.
default_spf="v=spf1 a mx ~all"
; Default DMARC record added for newly created domains. Note, if empty no record is created.
default_dmarc="v=DMARC1; p=none"

; General database tweaks
[database]
; Maxmimum connections permitted to a database server (MySQL) or database (PostgreSQL). This
; is presented as a global tune. Individual overrides in [mysql] and [pgsql] take precedence.
concurrency_limit=20
; Default span to backup databases
backup_span=5
; Number of backups to preserve
backup_preserve=2
; Default extension
backup_extension=zip

[mysql]
; ProxySQL in front of MySQL. Requires updating authentication on both ends.
; Only available for localhost/127.0.0.1
proxysql=false
; Limit connections per user to server. Setting to higher numbers
; may mask underlying problems (over quota, poorly optimized queries).
; 10 is suitable for sites serving > 250 GB/month
; If unset the value is inherited from [database] => concurrency_limit
concurrency_limit=

[pgsql]
; Limit active connections to a database. Unlike MySQL, enforcement is per database
; instead of per user. Setting to a high value may mask underlying problems as above.
; If unset the value is inherited from [database] => concurrency_limit
database_concurrency_limit=

[quota]
; Storage multiplier if over quota
storage_boost=2
; Time in seconds amnesty is applied
storage_duration=43200
; Min wait time, in seconds, between requesting amnesty
storage_wait=2592000

[domains]
; Nameserver verification check before allowing a domain
; to be added. Enable on multi-user setups to prevent a user
; from adding google.com and routing all server mail for
; google.com to the user account.
dns_check=true
; Notify admin whenever a domain is added to any account.
; Setting dns_check and notify to false is only recommended
; on a single-user installation.
notify=false

[ssh]
; Include embedded Terminal for users
embed_terminal=true
; Enable users to run daemons
user_daemons=true
; Default theme to use for shellinabox
theme=default
; Enable a limited set of sudo actions (/bin/rm) for account admins to run
; Doing so would give access to remove multiPHP interpreters + system sockets
; linked/shared in /.socket.
; Only enable if you can absolutely trust your users are not malicious.
; Requires installation of sudo package. (yum-post.php install sudo ssh)
sudo_support=false

[auth]
; When using a multi-server reverse proxy, use this URL
; to query the domain database server
; See https://github.com/apisnetworks/cp-proxy
;  +  Auth::Redirect
server_query=
; When redirecting a login request elsewhere, format the
; redirection as this FQDN, e.g.
; if server = foo and server_format = <SERVER>.apiscp.com, then
; redirect: foo.apiscp.com
; Leaving blank implies SERVER_NAME
; Setting server_format is necessary for <username>/<server> support
server_format=
; Before redirection perform a DNS check on the server name
; Requires [dns] => recursive_nameserver to report valid entries for
; formatted domain name (via server_format)
server_validity=false
; Minimum acceptable password length
min_pw_length=7
; Force password requirements check, implies min_pw_length
pw_check=true
; Allow admin API commands, add/delete/edit/suspend/activate/hijack
; Disable to provide added security if a permission exploit were discovered
admin_api=true
; Allow sites whose billing,invoice matches another site's biling,parent_invoice
; to SSO into the account
subordinate_site_sso=true
; Allow suspended accounts the ability to login to the panel?
suspended_login=true
; show suspension reason in UI if present and account rejected
show_suspension_reason = true
; Retain password in session for SSO to webmail
retain_ui_password=true
; Special key to encrypt all seen sessions. In multi-server setups this value
; MUST be the same across ALL servers. On new installs this is set automatically
; by the Bootstrapper
secret=
; Changes that affect self-service under Account > Summary
; Allow username changes
allow_username_change=true
; Allow domain changes
allow_domain_change = true
; Allow database prefix changes
allow_database_change = true
; Maximum number of IP restrictions an account may set.
; Includes range and single addresses. Set to -1 to cap
; to global limit (50). Set to 0 to disallow users from setting
ip_restriction_limit=50
; On password reset, implicitly update an IP restriction.
; Valid options are true or false.
; A hijacked email account on the same account allows an attacker
; unlimited control when true.
update_restrictions_on_reset=false

[billing]
; All accounts attached with this invoice are considered "demo" and restricted.
demo_invoice=APNS-HOSTING-1111111111111111

[antivirus]
; ClamAV is installed on system
installed=true

[misc]
; Base URL for all support articles. If you would like to self-host
; contact license@apisnetworks.com for information on mirroring KB
kb_base=https://kb.apiscp.com
; In multi-panel installations, use cp_entry as reverse proxy
; See https://github.com/apisnetworks/cp-proxy
cp_proxy=
; Aggregate system status portal used in login portal. Requires Cachet
; See https://cachethq.io and set to URL before api/
sys_status=

[screenshots]
; chromedriver is installed and can capture website screenshots to facilitate visual recognition
; Run yum install -y chromedriver chromium
; adds ~400 MB files
enabled=false
; Acquisition method, self-hosted or remote. "self" for now.
acquisition=self
; Duration in seconds a screenshot should be retained before being recaptured
ttl=172800
; Run at most n screenshots every cron period. Primarily intended to deflect self-induced DoS
; Setting -1 would force a bulk update every TTL seconds
batch=200
; chromedriver host. Leave 0 for random, OS-assigned port. Only 127.0.0.1 is supported.
; If a port is specified, it must be > 1024
chromedriver="http://127.0.0.1:0"

[telemetry]
; Enable platform learning
enabled=true
; Auto-tune database on extension update. Autotuning invokes
; timescaledb-tune on each package update, adjusting WAL and memory
autotune=true
; Maximum memory consumption. Leave undefined for autodetection.
; When unit omitted, assumed MB.
memory_consumption=
; Maximum WAL consumption. Leave undefined for autodetection.
; Used for durability/replication across nodes
; When unit omitted, assumed MB.
max_wal=
; Perform routine compressions on metric data older than timespec
; Values older than this are averaged out into COMPRESSION_CHUNK windows
; Compression values are organized left-to-right decreasing from CURRENT_TIMESTAMP
;
; "2 day, 7 day" means 2 days from today, then 7 days from today
; "now, 7 day" would be today, then 7 days from today effectively compressing all data each run.
compression_threshold=2 days, 7 days
; Chunk data older COMPRESSION_THRESHOLD
compression_chunk=5 minutes, 1 hour
; Archival compression prevents modification of data past 48 hours
; Requires decompression before sites may be deleted
archival_compression=false
; Merge duplicate records by value older than compression_chunk
merge_duplicates=false
; Maximum number of chunks to archive in one batch. Adjust lower if metricscron
; report "(54000) ERROR: too many range table entries"
archival_chunk_size=100


;;;
;;; Cron
;;;
[cron]
; Minimum cron resolution time, in seconds, for apnscpd. Set to 0 to disable
; cron feature, which will block job runner + _cron() tasks.
resolution=60
; Maximum number of workers, each worker takes up between 24-32 MB
max_workers=1
; Disable Horizon and use a primitive single-runner queue manager, frees up 40-60 MB
low_memory=false
; As a percentage of run-queue capacity. Run if 1-minute load < <CPU Count> * <LOAD_LIMIT>
load_limit=0.75
; Disable cron/housekeeping in debug mode. Running backend in debug
; mode can expose routine tasks to different outcomes.
no_debug = false

;;;
;;; Account management
;;;
[opcenter]
; default plan name, symlinks from plans/.skeleton
default_plan="basic"
; Configuration directives not listed in plans/default/<svc>
; will terminate execution
strict_svc_config = true
; PROTECTED
; Relative to resources/ or an absolute path
plan_path = templates/plans
; require IP addresses be bound to the server before allocating to site
ip_bind_check=true
; initial site ID offset for account IDs. Must be [0,32767)
site_id_offset=0

;;;
;;; Server brute-force deterrent
;;;
[rampart]
; Default jail prefix for all fail2ban jails`
prefix = "f2b-"
; Default driver for rampart, iptables or ipset
driver = ipset
; Allow up to n whitelisted IPs per site.
; -1 allows unlimited, 0 disables, n > 0 to cap
delegated_whitelist = 0
; Show reason why client is banned from logfile
show_reason = true
; Change delegation set name. Set name must exist. Possible choices: whitelist or ignorelist
delegation_set = ignorelist
; Allow User Administrators to unban self. If disabled, only Site Administrators may unban self
user_discovery = true

;;;
;;; Enable FTP services
;;;
[ftp]
; FTP is enabled. Triggered via ftp.enabled Scope
enabled = true
; Only allow SSL connections via STARTTLS.
; Affects local connections too!
ssl_only = false

;;;
;;; Account resource enforcement
;;;;
[cgroup]
; PROTECTED
; location for cgroup controllers
home="/sys/fs/cgroup"
; PROTECTED
; default controller support
controllers=memory,cpu,cpuacct,pids,blkio,cpuset,freezer
; Time in seconds to cache "admin:get-usage cgroup"
; On a 650 account server aggregating 24 hour data,
; ~1.5m records, takes ~45 seconds. cron periodically checks for
; cache presence and freshens as necessary.
; Setting to "0" effectively disables this
prefetch_ttl=3600
; Enable showing of cgroup usage in Nexus. Affects
; prefetch task in cron.
show_usage=true

;;;
;;; Apache
;;;
[httpd]
; PROTECTED
; Bind to all available interfaces.
; Disabling this feature requires manual reconfiguration in httpd-custom.conf.
; Each namebased IP address must be explicitly defined in a Listen directive when disabled.
; Disabling allows another service to bind to 80 on a non-namebased IP.
; Disabling this greatly affects Apache throughput.
all_interfaces=true
; Window to allow multiple HTTP build/reload requests
; to coalesce. Set to "now" to disable.
reload_delay='2 minutes'
; Place all sites in PHP-FPM jails by default
; Each account occupies approximately 40 MB
use_fpm=true
; Strip non-jailed path when converting site from mod_php to PHP-FPM
; Controls migrating php_value to .user.ini as well
fpm_migration=true
; Allow users to switch between system user and self
fpm_user_change=true
; Control PrivateTmp usage. When disabled, /tmp is relative to siteXX/fst/tmp and may
; be read by other users on the account. When enabled, /tmp is on system mountpoint as tmpfs
; and disables binary execution. Unless a site receives a high volume of uploads, keep this enabled.
fpm_privatetmp=true
; Default port for non-SSL connections. Leave empty disable
; Secondary configuration in httpd-custom.conf required
nossl_port=80
; Default port for SSL connections. Leave empty disable
; Secondary configuration in httpd-custom.conf required
ssl_port=443
; Enable per-site storage for Pagespeed in siteXX/fst/var/cache/pagespeed
; If enabled, all Pagespeed optimizations are charged to a site otherwise
; optimizations are charged anonymously to "apache"
pagespeed_persite=false

;;;
;;; Server-to-server xfer
;;;
[migration]
; During migrations from "transfersite.php", send notices from this email.
status_email = apnscp@${HOSTNAME}

;;;
;;;
;;;
[webapps]
; Comma-delimited list of app names to blacklist
blacklist = magento
; A percentage error between 0 and 100 of the permitted content length deviation
; between updates. Calculated as |(actual - expected)/expected| * 100
assurance_drift=5
; Composer can use an unprecedented amount of memory during package dep solves such
; as with Laravel or Nextcloud. Allow ApisCP to temporarily remove memory limits on
; the account to ameliorate potential OOM conditions if the function call
; happens in backend.
composer_volatile=false
; Use the following default email notification policy. This may be overridden on a per-account
; basis via Account > Settings > Apps > Web Apps
; Possible values:
;   "always" always send notices
;   "none"   never send notices
;   "fail"   send notices only on failure
notify_update=always

;;;
;;;
;;;
[bandwidth]
; Bandwidth is rounded down and binned every n seconds
; Smaller resolutions increase storage requirements
resolution=180
; Bandwidth stopgap expressed in percentage
; 100 terminates a site when it's over alloted bandwidth
; Default setting is 200 which suspends the site when it has exceeded 200% its bandwidth
; 95 would suspend a site when it's within 5% of its bandwidth quota
stopgap = 125
; Bandwidth notification threshold expressed in percentage
; As a sanity check, bandwidth_notify < bandwidth_stopgap
; Setting 0 would effectively notify every night
notify = 90

;;;
;;; Reseller support (experimental)
;;;
[reseller]
;;; Enable reseller support (experimental)
enabled=0
