---
title: Authors
alias: authors
---

apnscp is comprised of several components. Any component under vendor/ is 
copyright the respective author. All software that is modified under GPL 
is available under https://github.com/apisnetworks.

Code under different locations has different licenses:
- lib/modules
  * Artistic 2.0
- lib/HTTP
  * BSD
- lib/Util/Process
  * MIT
- resources/playbooks
  * MIT 
- build/bootstrap/apnscp-bootstrap-jdk
  * MIT
- build/bootstrap/apnscp-bootstrap-sdk
  * MIT
- apnscp root 
  * apnscp Proprietary License (refer to LICENSE)
  

All unlicensed content is assumed to be licensed under apnscp's proprietary license unless specified.
