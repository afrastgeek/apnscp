<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-control-label">
		<i class="fa fa-rocket"></i>
		Control Panel
	</h5>
	<div class="col-sm-6">
		<label class="pl-0 form-inline custom-checkbox custom-control mt-1 align-items-center d-flex">
			<input type="checkbox" class="custom-control-input" name="cp_enable" id="cp_enable" value="1"
					{{ $Page->get_option('cp_enable', 'checkbox') }} />
			<span class="custom-control-indicator"></span>
			Enable
		</label>
	</div>
</fieldset>