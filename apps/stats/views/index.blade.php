@php
    $processor = $Page->getProcessors();
@endphp

<div id="accordion" role="tablist" aria-multiselectable="true">
	@include('partials.os')
	@include('partials.hardware')
	@include('partials.network')
	@include('partials.memory')
	@include('partials.filesystem')
	@includeWhen($sastats = $Page->spamassassin(), 'partials.spamassassin', ['sastats' => $sastats])
</div>
