<div class="head5 right task-switcher">
	@if (isset($_GET['hostname']))
		<a class="ui-action ui-action-switch-app ui-action-label"
	    href="{{ \HTML_Kit::page_url_params(array('hostname' => null)) }}">Return to domain listing</a>
	@endif
</div>

@if ($Page->getMode() === 'edit' && $Page->getPersonalityPath())
	@include('partials.edit', [
		'target'      => $Page->getPersonalityTarget(),
		'controlFile' => $Page->getControlFile(),
		'personalities' => $Page->scan($Page->getControlFile())
	])
@else
	@include('partials.list')
@endif