<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\prlanguages;

	use Page_Container;
	use Web_Module;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->add_css('prlanguages.css');
			$this->add_script(array('/js/ajax_unbuffered.js'), false);
		}

		public function on_postback($params)
		{

		}

		public function ssh_enabled()
		{
			return $this->getApnscpFunctionInterceptor()->common_service_enabled('ssh');
		}

		public function subdomain_enabled($mUser)
		{
			return $this->getApnscpFunctionInterceptor()->web_user_service_enabled($mUser, 'subdomain');
		}

		public function toggle_ror()
		{

		}

		public function getLocalRORVersion()
		{
			$pack = $this->getApnscpFunctionInterceptor()->web_list_installed_gems();

			return isset($pack['rails']['version']) ? $pack['rails']['version'] : 'Not Installed';
		}

		public function getRemoteRORVersion()
		{
			$pack = $this->getApnscpFunctionInterceptor()->web_list_remote_gems();

			return @(array_shift($pack['rails']['versions']));
		}

		public function getUsers()
		{
			return $this->getApnscpFunctionInterceptor()->user_get_users();
		}

		public function rubyOnRailsEnabled()
		{
			return $this->getApnscpFunctionInterceptor()->web_ruby_on_rails_enabled();
		}

		public function needUpgrade()
		{
			return $this->web_get_gem_version('rails', true) != $this->web_get_gem_version('rails', false) ||
				Web_Module::RUBY_VERSION_FULL != $this->web_get_ruby_version(true);
		}

		public function getRubyInstalled()
		{
			return $this->web_get_ruby_version(true);
		}

		public function getRubyAvailable()
		{
			return Web_Module::RUBY_VERSION_FULL;
		}
	}

?>
