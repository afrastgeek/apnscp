<?php
?>
<!-- main object data goes here... -->
<form method="POST" action="spf?domain=<?=$Page->getDomain()?>" name="spf">
	<h4 class="form-inline">
		SPF Record Setup for
		<select name="domain" class="ml-1 form-control custom-select form-control-inline "
		        OnChange="document.location='/apps/spf?domain=' + this.value;">
			<?php
		    foreach($Page->list_domains() as $domain) {
			print '
			<option
			'.(isset($_GET['domain']) && $_GET['domain'] == $domain ? 'SELECTED ' :
			'').'name="'.$domain.'">'.$domain.'</option>';
			}
			?>
		</select>
	</h4>
	<?php
    $ipaddress = $Page->getIPAddress();
	if (!$ipaddress) {
	?>
	<p class='alert alert-info'>
		No DNS records found for hostname &quot;<?= $Page->getDomain() ?>&quot;. DNS is probably
		not setup correctly.
	</p>
	<?php
        return;
    }
    ?>

	<table width="100%">
		<tr>
			<td class="cell1">
				<INPUT TYPE="HIDDEN" NAME="mydomain" VALUE="<?=$Page->getDomain();?>">
				<TABLE ID="SPF-Wizard">
					<?php
		    $spfExists = $Page->getSpf();
					if (!empty($spfExists)):
					?>
					<TR>
						<TD CLASS="warning" COLSPAN="3">
							<div class="alert alert-warning">
						<span><strong>Warning:</strong> A SPF record already exists for your domain.  Clicking &quot;Save Record&quot; will
						overwrite this record:
						<code class="prior-spf"><?=$spfExists?></code>
						</span>
							</div>
						</TD>
					</TR>
					<TR>
						<TD CLASS="head3" COLSPAN=3 HEIGHT=1><img src="/images/spacer.gif" alt="" height=1/></TD>
					</TR>
					<?php
		    endif;
		?>
					<TR>
						<TD CLASS="cell1" COLSPAN=1>
							<?php $ipaddrs = $Page->getIPAddress(); ?>
							<?=$Page->getDomain(); ?>'s IP address<?php echo (count($ipaddrs) > 1 ? 'es are ' : ' is '),
							join(", ", $ipaddrs) ;?>.
							<?php print(sizeof($ipaddrs) > 1 ? 'Do those servers ' : 'Does this server '); ?> send mail
							from <em><?=$Page->getDomain(); ?></em>?
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[a]
						</TD>

						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
								<TR>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="a" VALUE="yes" CHECKED
									                                      onClick="update_rsf(this.form)"></TD>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="a" VALUE="no"
									                                      onClick="update_rsf(this.form)"></TD>
								</TR>
								<TR>
									<TD ALIGN=CENTER class="cell1">yes</TD>
									<TD ALIGN=CENTER class="cell1">no</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD CLASS="head3" COLSPAN=3 HEIGHT=1><img src="<?=APNSCP_DOC_ROOT?>/images/spacer.gif" alt=""
						                                          height=1/></TD>
					</TR>
					<TR>
						<TD CLASS="cell1" COLSPAN=1>
							<?php $mxrecords = $Page->getMXRecords(); ?>
							This wizard
							found <?php printf("%u name%s for the MX server%s for ", sizeof($mxrecords), (sizeof($mxrecords) >
							1 ? 's' : ''), (sizeof($mxrecords) > 1 ? 's' : '')); ?> <em><?=$Page->getDomain();?></em>:
							[<?php print join(', ', array_column($mxrecords, 'parameter')); ?>].
							(A single machine may go by more than one hostname. All of them are shown.)
							<BR>MX servers receive mail <I>for</I> <em><?=$Page->getDomain(); ?></em>.
							Do they also send mail <I>from</I> <em><?=$Page->getDomain(); ?></em>?
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[mx]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
								<TR>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="mx" VALUE="yes" CHECKED
									                                      onClick="update_rsf(this.form)"></TD>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="mx" VALUE="no"
									                                      onClick="update_rsf(this.form)"></TD>
								</TR>
								<TR>
									<TD ALIGN=CENTER class="cell1">yes</TD>
									<TD ALIGN=CENTER class="cell1">no</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD CLASS="head3" COLSPAN=3 HEIGHT=1><img src="/images/spacer.gif" alt="" height=1/></TD>
					</TR>
					<TR>
						<TD CLASS="cell1" COLSPAN=1>
							Do you want to just approve any host whose name ends in
							<em><?=$Page->getDomain(); ?></em>? (Expensive, unreliable and not recommended)
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[ptr]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
								<TR>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="ptr" VALUE="yes"
									                                      onClick="update_rsf(this.form)"></TD>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="ptr" VALUE="no" CHECKED
									                                      onClick="update_rsf(this.form)"></TD>
								</TR>
								<TR>
									<TD ALIGN=CENTER class="cell1">yes</TD>
									<TD ALIGN=CENTER class="cell1">no</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD CLASS="head3" COLSPAN=3 HEIGHT=1><img src="<?=APNSCP_DOC_ROOT?>/images/spacer.gif" alt=""
						                                          height=1/></TD>
					</TR>
					<TR>
						<TD CLASS="cell1" ROWSPAN=2 VALIGN=TOP>
							<P>Do any other servers send mail from <em><?=$Page->getDomain();?></em>? <br/>
								You can describe them by giving "arguments" to the <B>a:</B>, <B>mx:</B>, <B>ip4:</B>,
								and <B>ptr:</B> mechanisms.
								To keep the wizard short we left out <B>ptr:</B> but it works the same way.
							</P>
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[a:]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TEXTAREA NAME="a_colon" class="form-control" ROWS="3" onChange="update_rsf(this.form);">regular hostnames</TEXTAREA>
						</TD>
					</TR>
					<TR>
						<TD CLASS="cell1" ALIGN=CENTER>
							[mx:]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TEXTAREA class="form-control" NAME="mx_colon" ROWS="3" onChange="update_rsf(this.form);">MX servers</TEXTAREA>
						</TD>
					</TR>
					<TR>
						<TD CLASS="cell1" ALIGN=RIGHT>
							<BR><I>IP networks can be entered using CIDR notation, eg. 192.0.2.0/24</I>
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[ip4:]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TEXTAREA class="form-control" NAME="ip4_colon" ROWS="3" onChange="update_rsf(this.form);">IP addresses</TEXTAREA>
						</TD>
					</TR>
					<!-- </TR> -->
					<TR>
						<TD CLASS="cell1" COLSPAN=1>
							Could mail from <em><?=$Page->getDomain()?></em> originate through
							servers belonging to some other domain?
							<BR>If you send mail through your ISP's servers, and the ISP has
							published an SPF record, name the ISP here. <!-- </P> -->
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[include:]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<INPUT TYPE="text" NAME="include" class="form-control" VALUE="example.com"
							       onChange="update_rsf(this.form)">
						</TD>
					</TR>
					<TR>
						<TD CLASS="head3" COLSPAN=3 HEIGHT=1><img src="<?=APNSCP_DOC_ROOT?>/images/spacer.gif" alt=""
						                                          height=1/></TD>
					</TR>
					<TR>
						<TD CLASS="cell1" COLSPAN=1>
							Do the above lines describe <strong>all the hosts</strong>
							that send mail from <em><?=$Page->getDomain(); ?></em>?
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER>
							[-all]
						</TD>
						<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
							<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0>
								<TR>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="all" VALUE="yes" CHECKED
									                                      onClick="update_rsf(this.form)"></TD>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="all" VALUE="maybe"
									                                      onClick="update_rsf(this.form)"></TD>
									<TD ALIGN=CENTER class="cell1"><INPUT TYPE="radio" NAME="all" VALUE="no"
									                                      onClick="update_rsf(this.form)"></TD>
								</TR>
								<TR>
									<TD ALIGN=CENTER class="cell1">yes</TD>
									<TD ALIGN=CENTER class="cell1">maybe</TD>
									<TD ALIGN=CENTER class="cell1">no</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<!-- record-so-far -->
					<TR>
						<TD CLASS="cell1" COLSPAN=2 ALIGN=CENTER VALIGN=TOP>
							<h5>Proposed SPF Record</h5>
							<div class="input-group">
								<span class="input-group-addon"><?=$Page->getDomain();?>. IN TXT</span>
								<input type="text" NAME="record_so_far" class="form-control" id="spf_rec"/>
							</div>
						</TD>
						<TD CLASS="cell1 align-bottom" ALIGN=CENTER>
							<INPUT TYPE=HIDDEN NAME="use_built_from_args" VALUE="1">
							<INPUT TYPE=SUBMIT class="btn btn-primary" NAME="Save" VALUE="Save Record">
						</TD>
					</TR>
				</TABLE>
			</td>
		</tr>
	</table>
</form>
