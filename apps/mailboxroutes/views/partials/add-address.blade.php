<div class="row">
	<fieldset class="form-group">
		<label class="col-12">
			Delivery Type
		</label>
		<div class="col-12">
			<div class="btn-group form-inline" data-toggle="buttons" id="deliveryType">
				<div class="input-group mr-3 mb-1">
					<span class="input-group-addon ui-action ui-action-label ui-action-email-local"></span>
					<label class="btn btn-outline-secondary align-items-center d-flex
								@if (!isset($_POST['type']) || $_POST['type'] === "v") active @endif form-control-static"
					       data-toggle="tooltip">
						<input type="radio" class="radio-inline mr-2" data-toggle="radio-collapse"
						       aria-expanded="false"
						       @if (!isset($_POST['type']) || $_POST['type'] === "v") checked="CHECKED" @endif
						       data-parent="deliveryOptions" aria-controls="userContainer"
						       data-target="#userContainer"
						       id="type_single" name="type" value="v" checked="CHECKED"/>
						Single User
					</label>
				</div>
				<div class="input-group mb-1">
					<span class="input-group-addon ui-action-label ui-action ui-action-email-forward"></span>
					<label class="btn btn-outline-secondary d-flex align-items-center
							@if (isset($_POST['type']) && $_POST['type'] === "a") active @endif form-control-static"
					       for="type_forwarded" data-toggle="tooltip"
					       title="Email will be forwarded elsewhere or delivered to multiple addresses">
						<input type="radio" id="type_forwarded" class="radio-inline mr-2" name="type" value="a"
						       data-parent="deliveryOptions" data-toggle="radio-collapse"
						       aria-controls="forwardContainer"
						       @if (isset($_POST['type']) && $_POST['type'] === "a") checked="CHECKED"
						       @endif data-target="#forwardContainer"/>
						Forward or Multiple Users
					</label>
				</div>
			</div>
		</div>
	</fieldset>
</div>

<div class="panel-group" id="deliveryOptions">

	<div class="panel form-group collapse
			@if (!isset($_POST['type']) || $_POST['type'] === "v") show @endif" id="userContainer">
		<label for="mailbox">Deliver to this user account</label>

		<select name="mailbox" class="local-delivery form-control custom-select" id="mailbox">
			@foreach ($Page->get_users() as $user => $data)
				<option value="{{ $data['uid'] }}">
					{{ $user }}
				</option>
			@endforeach
		</select>
	</div>

	<div class="panel form-group clearfix row collapse
			@if (isset($_POST['type']) && $_POST['type'] === "a") show @endif" id="forwardContainer">
		<label class="col-12" for="alias_local">
			Include these local users
		</label>
		<div class="col-md-12 form-group">
			<select name="alias_local[]" id="alias_local"
			        multiple="multiple" class="local-delivery form-control" size="6">
				@foreach ($Page->get_users() as $user => $data)
					<option value="{{ $user }}">{{ $user }}</option>
				@endforeach
			</select>
		</div>

		<div class="panel form-group col-md-8 col-12 clearfix">
			<label>Include these external email addresses:</label>
			<label for="alias_remote" class="hinted">user@domain1.com, user@domain2.com, ...</label>
			<textarea name="alias_remote" id="alias_remote" rows="3" class="forward-delivery form-control"></textarea>
			<small>separate multiple addresses with commas</small>
		</div>

	</div>


</div>