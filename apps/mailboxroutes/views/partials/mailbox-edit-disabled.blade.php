<div class="btn-group">
	<a class="btn btn-secondary ui-action-label ui-action ui-action-enable" href="#"
	   onclick="return apnscp.assign_url('{{ HTML_Kit::new_page_url_params(null, ['mode' => $Page->getMode(), 'enable' => $addrHash]) }}');">
		Enable
	</a>
	<button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
	        aria-expanded="false"></button>
	<div class="dropdown-menu  dropdown-menu-right">
		<a class="ui-action ui-action-delete btn-block dropdown-item  btn warn ui-action-label"
		   title="delete" href="#"
		   onclick="return confirm('Are you sure you want to delete \'{{ $account['user'] . " @" . $account['domain'] }}\'?')
				   && apnscp.assign_url('{{ HTML_Kit::new_page_url_params(null, ['mode' => $Page->getMode(), 'd' => $addrHash]) }}');">
			Delete
		</a>
	</div>
</div>