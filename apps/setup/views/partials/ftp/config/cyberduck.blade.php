{{--
	@var \apps\setup\models\Authentication $auth
--}}
<? xml version = "1.0" encoding = "UTF-8"?>
		<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Protocol</key>
		<string>ftps</string>
		<key>Provider</key>
		<string>iterate GmbH</string>
		<key>UUID</key>
		<string>{{ DNS_UUID }}</string>
		<key>Hostname</key>
		<string>{{ $auth->hostname('ftp', true) }}</string>
		<key>Port</key>
		<string>21</string>
		<key>Username</key>
		<string>{{ $auth->username() }}{{ '@' }}{{ $auth->hostname('ftp') }}</string>
		<key>Path</key>
		<string></string>
	</dict>
</plist>