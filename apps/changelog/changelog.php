<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\changelog;

	class Page extends \Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->setTitle("Changelog");
			\Page_Renderer::hide_breadcrumbs();

		}

		public function _render()
		{
			$this->view()->share([
				'cp_rev'    => \Util_Conf::cp_version(),
				'changelog' => $this->getApnscpFunctionInterceptor()->misc_changelog()
			]);
		}

		public function on_postback($params)
		{

		}

		public function getChangelog()
		{
			return $this->getApnscpFunctionInterceptor()->misc_changelog();
		}
	}