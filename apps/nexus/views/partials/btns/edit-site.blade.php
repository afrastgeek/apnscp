<div class="row">
	<div class="col-12">
		<div class="btn-group">
			<button type="submit" name="edit" class="btn btn-primary ajax-wait">Edit Site</button>
			<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
			        aria-haspopup="true" aria-expanded="false">
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="#dropdownCertificateOptions">
				<button class="dropdown-item btn btn-block ui-action ui-action-label warn ui-action-restore" id="reset" type="submit"
				        name="reset" value="{{ $Page->getSite() }}">
					{{ _("Reset Defaults") }}
				</button>
			</div>

		</div>
	</div>
</div>