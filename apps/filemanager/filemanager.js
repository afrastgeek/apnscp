$(document).ready(function () {
	var prompt_unsaved = false, fmModal;

	if (__esprit_fm == 'edit_perms') {
		$('input[name="permission[]"]').bind('change',
			function () {
				if ($(this).prop('checked'))
					val = parseInt($('#permission_view').text(), 8) | parseInt($(this).val());
				else
					val = parseInt($('#permission_view').text(), 8) & ~parseInt($(this).val());
				$('#permission_view').text(Number(val).toString(8));

			}
		);

	} else if (__esprit_fm == 'list') {
		$('tr.entry').highlight();
		apnscp.hinted();
		bindRename($('td.actions a.ui-action-rename'));
		var clipboardToggled = 1, toggled = 1;
		$('#clipboard-selectall').click(function (e) {
			var $el = $('#clipboardMenu .clipboard-item :checkbox');
			if (clipboardToggled) {
				$el.each(function () {
					$(this).prop('checked', true)
					return true;
				});
			} else {
				$el.each(function () {
					$(this).prop('checked', false);
					return true;
				});
			}
			clipboardToggled ^= 1;
		});

		$('#select_all').change(function (e) {
			var $el = $('tr.entry td:first-child :checkbox');

			if (toggled) {
				$el.each(function () {
					$(this).prop('checked', true);
					summarize_folder($(this));
					return true;
				});
				$('tr.file td').addClass('ui-highlight');
			} else {
				$el.each(function () {
					$(this).prop('checked', false);
					summarize_folder($(this));
					return true;
				});
				$('tr.file td').removeClass('ui-highlight');
			}
			toggled ^= 1;
			$(this).prop('checked', !toggled);
		});

		/*$('#fm_table td[class!="actions"]').selectable({
			filter: 'td',
		}).on('selected', function (event, ui) {
				return false;
				$(event.target).closest('tr').addClass('ui-selected');
				return true;
		}).on('unselected', function (event, ui) {
				return false;
				$(event.target).closest('tr').removeClass('ui-selected');
				return true;
			}
		);*/
		$('tr.file td :checkbox').change(function () {
			summarize_folder($(this));
		});

		var sizeBox = document.getElementById('sizeBox'),
			progress = document.getElementById('progress');

		var uploader = new ss.SimpleUpload({
			dropzone: 'uploadContainer',
			dragClass: 'dragover',
			url: '/apps/filemanager?Upload&s=' + session.id + '&cwd=' + __esprit_cwd + '&id=' + upload_id,
			name: 'uploaded_file',
			button: "upload-file",
			method: "POST",
			dataType: 'json',
			progressUrl: "/apps/filemanager/filemanager-ajax.php?fn=upload_progress&s=" + session.id + '&id=' + upload_id,
			responseType: 'json',
			maxSize: MAX_SIZE,
			multiple: true,
			zIndex: 100,

			onSubmit: function (filename, extension) {
				// Create the elements of our progress bar
				var progress = document.createElement('div'), // container for progress bar
					bar = document.createElement('div'), // actual progress bar
					fileSize = document.createElement('div'), // container for upload file size
					wrapper = document.createElement('div'), // container for this progress bar
					//declare somewhere: <div id="progressBox"></div> where you want to show the progress-bar(s)
					progressBox = document.getElementById('progressBox'); //on page container for progress bars

				// Assign each element its corresponding class
				progress.className = 'progress progress-striped';
				bar.className = 'progress-bar progress-bar-success';
				fileSize.className = 'size';
				wrapper.className = 'wrapper';
				$('.alert-danger', '#' + this._opts.dropzone).remove();
				// Assemble the progress bar and add it to the page
				progress.appendChild(bar);
				wrapper.innerHTML = '<div class="name">' + filename + '</div>'; // filename is passed to onSubmit()
				wrapper.appendChild(fileSize);
				wrapper.appendChild(progress);
				progressBox.appendChild(wrapper); // just an element on the page to hold the progress bars

				// Assign roles to the elements of the progress bar
				this.setProgressBar(bar); // will serve as the actual progress bar
				this.setFileSizeBox(fileSize); // display file size beside progress bar
				this.setProgressContainer(wrapper); // designate the containing div to be removed after upload
				var opts = {};
				if ($('#upload_overwrite').prop('checked')) {
					opts['upload_overwrite'] = true;
				}
				opts[$('#upload_progress').attr('name')] = $('#upload_progress').val();
				this.setData(opts);
			},

			// Do something after finishing the upload
			// Note that the progress bar will be automatically removed upon completion because everything
			// is encased in the "wrapper", which was designated to be removed with setProgressContainer()
			onComplete: function (filename, response, btn, size) {
				if (!response) {
					alert(filename + 'upload failed');
					return false;
				} else if (!response.success) {
					return this._opts.onError.call(this, filename, null, 200, null, response, btn, size);
				}
				injectFile(filename, 'file', '#' + this._opts.dropzone, size);

			},
			onError: function (filename, type, status, statusText, response, uploadBtn, size) {
				if (status != 200) {
					// @TODO undefined offset top error
					return true;
				}
				$('#uploadContainer').append(
					$('<p>').text(filename + " failed: " + response.msg).addClass("alert alert-danger")
				);
				return false;
			}
		});

		$('a span.filename_truncated').parent().parent().parent().parent().mouseover(function () {
			$('span.filename_truncated', this).css('display', 'none');
			$('span.filename', this).css('display', 'block');
			$(this).mouseout(function () {
				$('span.filename', this).css('display', 'none');
				$('span.filename_truncated', this).css('display', 'inline');
			});
		});

	} else if (__esprit_fm == 'view_file') {

		var LE = new LineEndings(''),
			os = $('#eol-original').val()
		token = LE.os2Token(os),
			symbol = LE.symbolize(token);
		$('#EOL').text(os.ucwords() + " (" + symbol + ")");
		$('.eol-types :input').change(function (e) {
			$('.eol-container .marker').text($(this).val().ucwords());
		});
		$('#eol-original').val(os.toLowerCase());
	}

	if (__esprit_fm == 'list_archive' || __esprit_fm == 'list') {
		$('#browse_path').click(
			function () {
				var content = fmModal.clone(false, false);
				fmModal.modal('hide').on('hidden.bs.modal', function() {
					fmModal.modal('dispose');
					var browser = apnscp.explorer({
						selected: __esprit_cwd,
						filter: 'filter=file,' + (__esprit_fm == 'list_archive' ? '0' : '1') + ';chown,1;show,/;show,/var/www;show,/home;show,/usr/local',
						onSelect: function (file, b) {
							$('#selected_dir').text(file);
						}
					});
					$('#select_dir', browser).click(function () {
						$('#browse_target', content).val($("#selected_dir", browser).text());
						browser.modal('hide').on('hidden.bs.modal', function () {
							fmModal = content.modal('show');
							browser.modal('dispose');
							$('#file_browser').remove();
						});
						return true;
					});
				});

				return false;
			});
	}
	// @TODO Convert select to styled lists
	$('#folderJump').click(function () {
		var explorer = apnscp.explorer({filter: 'filter=file,0', selected: __esprit_cwd});
		$('#select_dir').click(function () {
			val = $('#selected_dir').text();
			explorer.close();
			window.location.href = window.location.pathname + '?f=' + val;
			return true;
		});
		return false;
	});

	$('#fm-commands').click(function (e) {
		var originalTarget = e.target.id;
		switch (originalTarget) {
			case 'Create_Directory':
			case 'Create_Link':
			case 'Create_File':
				fmModal = apnscp.modal([
					$($(event.target).data('modal-content')),
					$('<input type="hidden" name="cwd" />').val(__esprit_cwd),
				]);
				/*$('#modal').one('show.bs.modal', function (event) {
					var related = $(event.relatedTarget).data('modal-content');
					modal.find('.modal-data').empty().append($(related));
				});*/
				break;
			case 'DownloadRemote':
				fmModal = apnscp.modal($('.download-modal').show());
				break;
			default:
				return true;
		}
		if (!fmModal) {
			return true;
		}
		fmModal.modal('show').on('hide.bs.modal', function () {
			$(this).find(':submit').unbind('submit.apnscpcmd');
			return true;
		}).on('shown.bs.modal', function () {
			$(this).find(':text:visible:first').focus();
			return true;
		}).on('submit.apnscpcmd', function (event) {
			var $indicator = apnscp.indicator().addClass('ui-ajax-loading'),
				$this = $(this),
				/**
				 * apnscp.modal() updates div content, but we lose input elements
				 * appended afterwards??? (see input type="hidden" name="cwd"...)
				 *
				 * ensure input vars are extracted by dom by refetching #modal
				 */
				data = fmModal.find('.modal-content :input, .modal-content :submit').serializeArray(),
				params = apnscp.collapseInput(data),
				filename = $this.val();

			$this.append($indicator);
			params[$(this).attr('name')] = filename;
			$this.prop('disabled', true);
			var ajaxReq = apnscp.call_app_ajax(
				session.appId,
				'on_postback',
				params,
				{indicator: $indicator}
			);

			$.ajax(ajaxReq).done(function (e) {
				var type = 'dir';
				if (originalTarget === 'Create_File') {
					type = 'file';
				} else if (originalTarget === 'Create_Link') {
					type = 'link';
				}
				fmModal.modal('hide').on('hidden.bs.modal', function () {
					// refresh
					window.location.href = window.location.href;
				});
				// @todo
				//injectFile(filename, type, '#modal', 0);
			}).fail(function (e) {
			}).always(function () {
				$this.prop('disabled', false);
				$indicator.removeClass('ui-ajax-loading');
			});
			//$('form').submit();
		});
	})
});

function getPath(filename) {
	return __esprit_cwd + '/' + filename;
}

function parseSize(size) {
	var pos = size.indexOf(" ");
	var unit = size.substr(pos + 1);
	size = parseFloat(size.substr(0, pos));

	switch (unit) {
		case 'GB':
			size *= 1024;
		case 'MB':
			size *= 1024;
		case 'KB':
			size *= 1024;
		default:
			return size;
	}
}

var LineEndings = function () {
	var EOLDEFAULT = '\r\n';
};

function injectFile(filename, type, transferFrom, size) {
	// Stuff to do after finishing an upload...
	var addAfter, substituteFile = null,
		placement = 'before';
	$('#fm_table .entry').each(function () {
		if (!$(this).hasClass('ftype-' + type)) {
			return true;
		}
		var $fn = $(this).find('.filename'), filenamelc = filename.toLowerCase();
		if ($fn.text().toLowerCase() > filenamelc) {
			substituteFile = $fn.text();
			addAfter = $fn.closest('.entry');
			return false;
		}
	});
	if (addAfter == null) {
		addAfter = $('#fm_table .entry:last');
		placement = 'after';

	}
	if (!addAfter) {
		$("body").append($('<div id="modal-wait"><i class="ui-ajax-indicator ui-ajax-loading ui-ajax-loading-large"></i></div>'));
		setTimeout(function () {
			$.get(window.location.href, function (ret) {
				$('#modal-wait').remove();
				// replace with new page req instead of clobbering JS
				var newDoc = document.open("text/html", "replace");
				newDoc.write(ret);
				newDoc.close();
				return false;
			})
		}, 2500);
		return true;
	}

	$(transferFrom || 'body').effect("transfer", {to: addAfter}, 500, function () {

		var $clone = addAfter.clone().attr('class', 'entry ftype-' + type),
			substituteFile = addAfter.find('.filename').text(),
			$frag = $($clone.hide().get(0).outerHTML.replace(new RegExp(substituteFile + '\\b', 'g'), filename));
		$frag.find('.size').text(Math.round((size || 0) / 1024 * 100) / 100 + ' MB');
		$frag.find('.name img').attr('src', '/images/apps/filemanager/file.gif');
		var $delete = $frag.find('.ui-action-delete');
		$delete.attr('href', $delete.attr('href').replace(/\bd=.+$/, 'd=' + utoa(getPath(filename))));
		bindRename($frag.find('.ui-action-rename'));
		addAfter[placement]($frag.fadeIn('fast'));
	});
	return true;
}

function bindRename($sel) {
	$($sel).renameRow('span.filename', {selectAll: true, file: true, parentSelector: ".entry"});
}

LineEndings.prototype.detectLineEndings = function (str) {
	if (typeof str !== 'string') {
		return this.EOLDEFAULT;
	}

	var newlines = (str.match(/(?:\r?\n)/g) || []);

	if (newlines.length === 0) {
		return null;
	}

	var crlf = newlines.filter(function (el) {
		return el === '\r\n';
	}).length;

	var lf = newlines.length - crlf;

	return crlf > lf ? '\r\n' : '\n';
};

LineEndings.prototype.symbolize = function (token) {
	switch (token) {
		case '\r\n':
			return 'CRLF';
		case '\n':
			return 'LF';
		case '\r':
			return 'CR';
	}
};

LineEndings.prototype.os2Token = function (os) {
	switch (os.toUpperCase()) {
		case 'WINDOWS':
			return '\r\n';
		case 'UNIX':
			return '\n';
		case 'MAC':
			return '\r';
	}
};

LineEndings.prototype.os = function (symbol) {
	switch (symbol.toUpperCase()) {
		case 'CRLF':
			return 'Windows';
		case 'LF':
			return "Unix";
		case 'CR':
			return 'Mac';
	}
};

var n_selected = 0, n_size = 0;

function summarize_folder(t) {
	var size = parseSize(t.parent().siblings('td.size').text());
	if (t.prop('checked')) {
		n_selected++;
		n_size += size;
	}
	else {
		n_selected--;
		n_size -= size;
	}
	if (n_selected >= 1) {
		if (n_selected == 1) {
			$('#folder-unselected').hide();
			$('#folder-selected').show();
		}
		$('#selected-size').text((n_size / 1024 / 1024).toFixed(2));
		$('#selected-count').text(n_selected);
	} else if (n_selected < 1) {
		$('#folder-selected').hide();
		$('#folder-unselected').show();
	}
};

