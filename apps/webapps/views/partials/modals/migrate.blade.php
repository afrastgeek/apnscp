@component('theme::partials.app.modal')
	@slot('title')
		Rename Site
	@endslot
	@slot('id')
		migrateModal
	@endslot
	<div>
		<i class="ui-ajax-loading-large ui-ajax-indicator ui-ajax-loading"></i>
		<div class="body fade">
			<p>
				This website is serving documents as <b class="site-url">{{ $app->getHostname() . '/' . $app->getPath() }}</b>.
				The following locations also serve content from this document root, <b>{{ $app->getDocumentRoot() }}</b>.
				SSL will be automatically added as necessary.
			</p>
			<div class="input-group">
				<select name="migrate" class="custom-select custom-select-lg">
				</select>@if ($app->getPath())<div class="input-group-addon">/{{ $app->getPath() }}</div>@endif
			</div>
			<div class="help-block with-errors text-danger"></div>
			@if ($app->hasReconfigurable('duplicate'))
			<hr/>
			<p class="note text-info">
				Use the
				<b class="ui-action ui-action-clone ui-action-label">Duplicate Site</b> feature to relocate this website
				to another document root.
			</p>
			@endif
		</div>
	</div>
	@slot('buttons')
		<button type="submit" name="rename-hostname" class="btn btn-primary restore ajax-wait">
			<i class="fa fa-pulse d-none"></i>
			Rename Site
		</button>
		<p class="ajax-response"></p>
	@endslot
@endcomponent