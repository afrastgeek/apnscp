<button data-note-target="#detectNote" type="submit" class="mb-3 ajax-wait btn btn-secondary"
        name="detect" value="{{ $app->getDocumentMetaPath() }}">
	<i class="fa fa-eye"></i>
	Detect
</button>