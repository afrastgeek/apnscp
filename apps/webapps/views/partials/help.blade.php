<div class="ui-help p-3">
	<h5>Help Topics</h5>
	<hr />
	<h6>Development</h6>
	<dl>
		@if ($app->allowGit())
			@if (!$app->hasGit())
				<dt>Enable Snapshots</dt>
				<dd>
					Place the application in a Git repository. <a href="https://git-scm.com/">Git</a> allows you to
					take periodic, differential snapshots of your site (database included) and restore from these backups.
					Additionally, Git provides a means to calculate differences between snapshots quickly informing you of
					changes
					(Terminal required).
				</dd>
			@else
				<dt>Manage Snapshots</dt>
				<dd>
					Create or restore the Web App from an existing snapshot. Resetting will destroy all data
					on the site.
				</dd>
			@endif
		@endif
	</dl>
	<h6>Access</h6>
	<dl>
		@if ($app->hasRecovery())
			<dt>Recovery Mode</dt>
			<dd>
				Attempt to load the minimal working copy of your application.
				Disable all non-essential plugins for the web application.
			</dd>
		@endif

		@if ($app->hasChangePassword())
			<dt>Change Admin Password</dt>
			<dd>
				Change the administrative password as well as retrieve the admin
				username.
			</dd>
		@endif
	</dl>
	<h6>Security</h6>
	<dl>
		<dt>Change Fortification</dt>
		<dd>
			If supported, <i>Release Fortification</i> permits write-access to all locations. <i>Fortify</i>
			disallows write-access to all folders except a predefined subset. <i>Web App Write Mode</i> is
			similar to
			<i>Release Fortification</i> except that after 10 minutes, all files created by the web server
			are locked
			down and no longer writeable by the web server. Useful for PHP applications that perform
			in-place
			updates that are not supported in this control panel. <i>Learning Mode</i>, if available, allows
			the control panel to learn access behavior of an unknown web app.
			<p class="mt-1">See KB: <a
						href="{{ MISC_KB_BASE }}/platform/understanding-fortification"
						class="ui-action ui-action-label ui-action-kb">Understanding fortification</a></p>
		</dd>

		<dt>Audit</dt>
		<dd>
			Generate a list of files owned by the web server under this domain or files that has sufficient
			write-access to allow write by the web server.
		</dd>

		@if (ANTIVIRUS_INSTALLED)
		<dt>Virus Scan</dt>
		<dd>
			Perform a scan of {{ $app->getDocumentMetaPath() }} for suspicious files.
		</dd>
		@endif
	</dl>
	<h6>Behavior</h6>
	<dl>
		<dt>Manifest</dt>
		<dd>
			Manifests augment Web App features. A Manifest for this app is located in
			<code>{{ $app->getManifest()->getManifestPath() }}</code>. After modifying the Manifest, it must be resigned
			using <b>Sign Manifest</b>.
		</dd>

		<dt>Detect</dt>
		<dd>
			Attempt to discover what application resides within the folder.
			New options, including Upgrade, Recovery, and Uninstall will become available
			if supported by the control panel.

			<b>Detection can also help unfreeze an app stuck in the &quot;Installing&quot; stage.</b>
		</dd>

		@if ($app->hasReconfigurable('migrate'))
			<dt>Rename Site</dt>
			<dd>
				Change the URL encoded in all links in the website to a new URL.
			</dd>
		@endif

		@if ($app->hasUpdate())
			<dt>Update</dt>
			<dd>
				Update the application and all of its plugins and/or themes to the latest
				version. <i>Note: this will be automated in future versions.</i>
			</dd>
		@endif

		@if ($app->hasUninstall())
			<dt>Uninstall</dt>
			<dd>
				Remove the database and database user created for use with the web application.
				Optionally remove all files created by the application.
			</dd>
	</dl>
	@endif
</div>
<div class="col-12 mb-1" id="help-topics">
	<p class="help" id="fortifyNote">
		Allow or revoke write privileges to this application. By revoking write-access, the risk of a
		hack becomes very small. Likewise,
		enabling write-access may allow an attacker to manipulate files on this account.
	</p>
	<p class="help" id="recoveryNote">
		Place your application into safe mode by disabling all third-party components. You will then be
		able to manually
		enable each component.
	</p>
	<p class="help" id="uninstallNote">
		Delete this application and its corresponding database
	</p>
	<p id="detectNote" class="help">
		Detection attempts to determine what app is installed at a given location
		<small>(only WordPress is supported at this time)</small>
	</p>
	<p id="upgradeNote" class="heLp">
		An update will update all components. Login to the application to update only a specific
		portion.
	</p>
	<p id="resetNote" class="help">
		Lost your password? Reset it to a new password.
	</p>
</div>