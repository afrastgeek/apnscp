<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
	 */


	namespace apps\scopes\models\scopes\Apache;

	use apps\scopes\models\Scope;
	use apps\scopes\RenderAsHtmlTrait;


	class Ports extends Scope
	{
		use RenderAsHtmlTrait;
		// need better way of managing this dynamically...

		public function getHelp()
		{
			$help = parent::getHelp() .
				'<br />After changing ports run <code>EditDomain --reconfig --all</code>. ' .
				'A panel restart occurs during port update.';

			return $this->asHtml($help);
		}
	}