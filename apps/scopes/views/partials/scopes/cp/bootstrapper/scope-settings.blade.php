<style type="text/css">
	#bootstrapperActions .position-absolute{
		right:0;
		top: 0;
	}

	#bootstrapperActions .position-absolute > div {
		min-width: 350px;
	}

	#submitScope {
		display: none;
	}
</style>

<div id="bootstrapperActions" class="align-content-end">
	<div class="d-block d-md-none mb-3 flex-sm-row flex-column">
		@include('partials.scopes.cp.bootstrapper.navigator')
	</div>
	<div class="position-absolute d-md-flex d-none">
		@include('partials.scopes.cp.bootstrapper.navigator')
	</div>
</div>