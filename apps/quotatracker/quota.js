var predictionValue = '.95', MIN = {x: new Date().getTime(), y: 0}, MAX = {x: 0, y: 0};

$(document).ready(function () {
	var data = apnscp.call_app(null, 'getStorageData', [], {dataType: "json"}).then(function (data) {
		drawChart(data);
	})

});

function drawChart(data) {
	var points = [], max = 0, ciRange = null,
		total, date, grouping = [];
	for (var i in data) {
		var user = data[i];
		if (!data[i]['data'].length) {
			continue;
		}
		grouping = [];
		for (var j in data[i]['data']) {
			//total = parseFloat(data[i]['quota']);
			max = Math.max(max, total);
			date = parseInt(data[i]['data'][j]['ts']) * 1000,
				point = [date, parseFloat(data[i]['data'][j]['quota']) / 1024];
			grouping.push(point);
		}
		MIN.x = Math.min(MIN.x, grouping[0][0]);
		MAX.x = Math.max(MAX.x, grouping[j][0]);
		MIN.y = Math.min(MIN.y, grouping[0][1]);
		MAX.y = Math.max(MAX.y, grouping[j][1]);
		points.push({
			label: user,
			color: data[i]['color'],
			lines: {show: true},
			data: grouping
		});
	}

	$.plot('#storage-graph', points, {
		series: {},
		legend: {
			show: false,
			margin: 0
		},
		xaxis: {
			min: MIN.x,
			max: MAX.x,
			mode: "time",
			minTickSize: [1, "month"]
		},
		yaxis: {
			min: 0,
			max: MAX.y * 1.03, /** add 3% */
			tickFormatter: function (x) {
				return x + " MB";
			},
			labelWidth: 60
		},
		zoom: {
			interactive: true
		},
		grid: {},
		pan: {
			interactive: true
		}
	});
}