<form id="traceroute_form" method="post">
	<p class="note">
		Your remote IP, to perform a reverse, traceroute is <code><?php print \Auth::client_ip(); ?></code>. The server
		IP
		address is <code><?php print $_SERVER['SERVER_ADDR']; ?></code>.
	</p>
	<div class="row">
		<label class="col-12">Target IP or Hostname:</label>
		<fieldset class="form-group col-12 form-inline">
			<div class="btn-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-laptop"></i></span>
					<input type="text" id="destination" name="destination" class="form-control"
					       value="<?php print(\Auth::client_ip()); ?>"/>
				</div>
				<button type="submit" class="btn btn-primary">
					Perform Traceroute
				</button>
			</div>
		</fieldset>
	</div>

	<div class="row">
		<div class="col-12">
			<span class="ui-ajax-indicator" id="loading_img"></span>
			<div style="color:red; display: inline;" id="status">&nbsp;</div>
			<div id="output_status_container" style="height:200px; overflow:scroll;"><code id="output_status"
			                                                                               class="prg-output"></code>
			</div>
		</div>
	</div>
	<?php
        if ($Page->is_postback && !$Page->errors_exist()): ?>
	<h4>Results</h4>
	<code>
		<pre><?php print($Page->getWhoisRecord($_POST['domain']));?></pre>
	</code>
	<?php
        endif;
    ?>

</form>
