<p>
	Node packages may be installed from
	<a href="https://www.npmjs.com/" class="ui-action ui-action-label ui-action-visit-site">npmjs.com</a>
	using npm.
	@if (version_compare(platform_version(), '6.5', '>='))
		It is strongly recommended to select a <a class="ui-action ui-action-kb ui-action-label"
		                                          href="<?=MISC_KB_BASE?>/node/changing-node-versions/">Node version</a>
		other than system default.
		<a href="https://github.com/nodejs/LTS">LTS</a> is typically recommended for maximum compatibility with packages
		on npm.
	@endif
</p>
<div class="mb-3">
	<p class="example">
		Example: changing Node versions:
	</p>
	<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
	    <code class="language-bash"># List remote Nodes
	nvm ls-remote
	# Install LTS release "boron" (6.11)
	nvm install lts/boron
	    </code>

	</pre>
</div>

<div class="mb-3">
	<p class="example mt-1">
		Example: to install <a href="https://www.npmjs.com/package/ghost"
		                       class="ui-action ui-action-label ui-action-visit-site">ghost</a>,
	</p>
	<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
	    <code class="language-bash">npm install ghost</code>
	</pre>
</div>