<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\vacation;

	use Opcenter\Mail\Vacation;
	use Page_Container;

	class Page extends Page_Container
	{
		protected $driver;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('vacation.css');
			$this->add_javascript('vacation.js');
			$this->driver = Vacation::get();
		}

		public function _layout()
		{
			parent::_layout();
			$this->view()->share(['optDriver' => $this->driver]);
			if ($this->driver->getOption('contenttype')->getValue() === 'html') {
				$this->initializeEditor();
			}
		}

		protected function initializeEditor()
		{
			$this->init_js('tinymce');
			$plugins = array(
				"spellchecker",
				"pagebreak",
				"layer",
				"table",
				"save",
				"tabfocus",
				"advlist",
				"emoticons",
				"searchreplace",
				"insertdatetime",
				"preview",
				"media",
				"searchreplace",
				"print",
				"contextmenu",
				"paste",
				"image",
				"directionality",
				"fullscreen",
				"noneditable",
				"visualchars",
				"nonbreaking",
				"template",
				"autoresize"
			);

			$this->add_javascript('$("#vacation_msg").tinymce({
				script_url: "/js/tiny_mce/tinymce.min.js",
				mode : "textareas",
			    theme : "modern",
			    plugins : "' . implode(",", $plugins) . '",
				schema: "html",
                verify_html: false,
				autoresize_max_height: 600,
                // Theme options
		        theme_modern_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		        theme_modern_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,tablecontrols",
		        theme_modern_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage,|,fullpage",
		        theme_modern_toolbar_location : "top",
		        theme_modern_toolbar_align : "left",
		        theme_modern_statusbar_location : "bottom",
		        theme_modern_resizing : true,
		        // Skin options
			    theme_advanced_toolbar_location : "top",
			    theme_advanced_toolbar_align : "left",
			    theme_advanced_statusbar_location : "bottom",
			    theme_advanced_resizing: true,
			    add_unload_trigger : true,
		        remove_linebreaks : false,
		        inline_styles : false,
		        tab_focus: ":prev,:next",
		        readonly: ' . intval(!$this->vacation_exists()) . '

            });
		', 'internal', false, false);
		}

		public function vacation_exists(): bool
		{
			return $this->getApnscpFunctionInterceptor()->email_vacation_exists();
		}

		public function on_postback($params)
		{
			if (!isset($params['save']) && !isset($params['test'])) {
				return;
			}

			if ($params['vacation']) {
				$this->setOptions($params);
				if (!$this->vacation_exists()) {
					$this->email_enable_vacation();
				}
			} else if (empty($params['vacation'])) {
				$this->email_remove_vacation();
			}
			$this->driver = Vacation::get($this->getAuthContext());
			$this->view()->share(['response' => $this->sendTest()]);
		}

		/**
		 * Set vacation flag options
		 *
		 * @param array $params
		 * @return bool
		 */
		protected function setOptions(array $params = []): bool
		{
			$defaults = $this->driver->getDefaults();
			$newopts = array_intersect_key($params, $defaults);
			$missing = array_diff_key($defaults, $params);

			return $this->email_set_vacation_options(array_merge($newopts, $missing));
		}

		protected function sendTest()
		{
			return $this->driver->test();
		}

		public function get_vacation_msg()
		{
			$msg = $this->email_get_vacation_message();
			$charset = array_get($this->email_get_vacation_options(), 'UTF-8');

			return htmlspecialchars($msg, ENT_QUOTES, $charset);
		}

		public function getVacationDefaults()
		{
			return $this->email_get_vacation_options();
		}
	}
