<form method="post" id="massDetectionForm">
	<fieldset class="">
		<button class="btn btn-secondary ajax-wait " type="button" name="scan-all" id="scanAll">
			<i class="fa fa-eye"></i>
			Detect All Hosts
		</button>
		<button class="btn btn-secondary ajax-wait " id="updateAll" disabled type="button" name="scan-all" id="scanAll">
			<i class="fa fa-arrow-circle-o-up"></i>
			Update All Apps
		</button>
		<label class="mb-0 align-middle">
			<input type="checkbox" name="show-detected" class="checkbox" id="showDetected"/>
			Display only detected hosts
		</label>
	</fieldset>
</form>
<hr/>
<?php

	$hosts = $Page->get_hostnames();
// we need this to expose map info
$dummyclass = new \Module\Support\Webapps\App\Loader();
$aliases = $Page->get_aliases();
$hash = array_fill_keys($aliases, "");
foreach ($hosts as $host => $hostpretty):
$href = \HTML_Kit::page_url_params(
array('hostname' => $host)
);
$docroot = \Util_Conf::call('web_get_docroot',$host);
$info = $dummyclass->getAppInfo($docroot);
$type = array_get($info, 'type', '');
$visithref = 'http://' . $hostpretty;
$css = $type ? "fa fa-" . $type : '';
?>
<div class="row mb-1 hostname">
	<div class="col-12 col-sm-6 col-md-5 col-lg-4 btn-group">
		<a class="ui-action ui-action-select ui-action-label btn btn-secondary" href="<?=$href?>">
			Select
		</a>
		<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
		        aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<div class="dropdown-menu" aria-labelledby="">
			<a class="ui-action-folder-browse ui-action-label dropdown-item ui-action" data-docroot="<?=$docroot?>"
			   data-hostname="<?=$host?>" href="#">
				Select Folder
			</a>
			<a class="ui-action-visit-site ui-action-label dropdown-item ui-action" href="<?=$visithref?>">
				Visit Site
			</a>
			<a class="ui-action-manage-files ui-action-label dropdown-item ui-action"
			   href="<?=HTML_Kit::new_page_url_params('/apps/filemanager', ['f' => $docroot]) ?>">
				Manage Files
			</a>
			<a class="fa fa-apache ui-action-label dropdown-item ui-action"
			   href="<?=HTML_Kit::new_page_url_params('/apps/personalities', ['hostname' => $host]) ?>">
				Edit .htaccess
			</a>
		</div>

		<div class="meta-inline form-control-static ml-1">
			<i title="<?=$type?>" class="app <?=$css?>"></i>
			<span class="version"></span>
		</div>
	</div>
	<div class="col-12 col-sm-6 col-md-7 col-lg-8 form-control-static <?=isset($hash[$host]) ? 'domain' : 'subdomain'?>">
		http://<?=$hostpretty?>
	</div>
</div>
<?php
	endforeach;
?>
