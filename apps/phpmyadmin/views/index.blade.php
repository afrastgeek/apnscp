<div class="row">
	<div class="col-12 col-md-6 offset-md-3">
		@includeWhen($Page->password_exists() && !$Page->failed, 'partials.sso-redirect' )
		@includeWhen(!$Page->password_exists() || $Page->failed, 'partials.sso-failed')
	</div>
</div>
