<label class="form-control-static">
	MaxMind GeoLite2 API Key
</label>
<fieldset class="form-group">
	@if ($user = $Page->getMaxMindKey())
		<button class="btn btn-secondary warn" name="delete-geolite-key" id="delete-geolite-key">
			<i class="ui-action ui-action-delete"></i>
			Delete
		</button>
		&lt;key hidden&gt;
	@else
		<input type="text" class="form-control" name="geolite-key" id="geolite-key"
		       value="{{ $user }}"/>
		<div class="alert alert-info" role="alert" id="origin-note">
			<i class="fa fa-sticky-note"></i> GeoLite2 key is required for Discourse usage.
			(see <a class="ui-action ui-action-label ui-action-kb"
			        href="https://dev.maxmind.com/geoip/geoip2/geolite2">GeoLite2 Free Downloadable Databases</a>)
		</div>
	@endif
</fieldset>