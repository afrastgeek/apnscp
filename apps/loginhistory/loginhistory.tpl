<!-- main object data goes here... -->
<table width="100%" class="table">
	<thead>
	<tr>
		<th width="40%"> Date
		</th>
		<th> IP (Host)
		</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = $Page->getData();
	$class = "cell1";
	foreach ($data as $item):
	?>
	<tr class="entry">
		<td class="cell1 <?php print($class); ?>">
			<?php print($item['ts']); ?>
		</td>
		<td class="cell1 <?php print($class); ?>">
			<?php
                $hostfrag = $item['host'] ? " (".$item['host'].")" : "";
                print($item['ip'] . $hostfrag);
            ?>
		</td>
	</tr>
	<?php
    if ($class == "cell1") $class = "even"; else $class = "cell1";
    endforeach;
    ?>
	</tbody>
</table>
