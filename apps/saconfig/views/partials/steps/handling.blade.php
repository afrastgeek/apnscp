<div class="row">
	<div class="col-12">
		<h4 class="step">Handling</h4>
		<ol class="pl-0 ml-3">
			<li>
				<label>
					I am using a(n) ______ inbox.
				</label>
				<ul class="list-unstyled">
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="mailbox_type" value="imap"
							       onChange="updateOpts(this);" class="custom-control-input"
							       @if ($mailbox_type == "imap") CHECKED @endif />
							<span class="custom-control-indicator"></span>

							<span>
								<a href="{{ HTML_Kit::page_url() }}/tip/imap" class="popover-trigger"
							        id="imap">IMAP</a>
								<b class="badge badge-info text-uppercase">recommended</b>
							</span>
						</label>
					</li>
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="mailbox_type" value="pop3"
							       onChange="updateOpts(this);" class="custom-control-input"
							       @if ($mailbox_type == "pop3") CHECKED @endif />
							<span class="custom-control-indicator"></span>
							<span>
								<a href="{{ HTML_Kit::page_url() }}/tip/pop3" class="popover-trigger"
							        id="pop3">POP3</a>
							</span>
						</label>
					</li>
				</ul>
			</li>

			<li>
				<label>
					How would you like to handle delivery of mail marked as spam?
				</label>
				<ul class="list-unstyled ml-1 pl-0">
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="delivery_mailbox" value="nothing" class="custom-control-input"
							       @if ($delivery_mailbox == "nothing") CHECKED @endif />

							<span class="custom-control-indicator"></span>
							<span>
								Take no action and deliver it normally
							</span>
						</label>
					</li>
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="delivery_mailbox" class="custom-control-input"
							       value="delete" @if ($delivery_mailbox == "delete") CHECKED @endif />

							<span class="custom-control-indicator"></span>
							<span>
								Permanently delete the message
							</span>
						</label>
					</li>
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="delivery_mailbox" class="custom-control-input"
							       value="move_spam"
							       @if ($delivery_mailbox == "move_spam") CHECKED @endif />

							<span class="custom-control-indicator"></span>
							<span>
								Move it to the mailbox named &quot;Spam&quot;
								<b class="badge badge-info text-uppercase">recommended</b>
							</span>
						</label>
					</li>
					<li>
						<label class="custom-control custom-radio mb-0 font-weight-normal">
							<input type="radio" name="delivery_mailbox" class="custom-control-input"
							       value="move_custom"
							       @if ($delivery_mailbox == "move_custom") CHECKED @endif />
							<span class="custom-control-indicator"></span>

							<span>
								Move it to my defined mailbox of
								<input type="text" class="form-control" name="move_custom_mailbox"
								       value="{{  $move_custom_mailbox }}" size=10/>
							</span>
						</label>
					</li>
					<li>
						<label class="custom-control custom-checkbox mb-0 font-weight-normal">
							<input type="checkbox" name="delivery_bias" data-toggle="collapse"
							       data-target="#biasAdvanced" value="1" class="custom-control-input form-control"
							       @if ($delivery_bias) CHECKED @endif />
							<span class="custom-control-indicator"></span>

							<span>
								<i>Optionally</i>, perform the following action if the mail scores above
								<input type="number" step="any" class="form-control" name="delivery_bias_score" maxlength=4
								       size=4
								       value="{{ $delivery_bias_score }}"/>
							</span>
						</label>
						<ul class="list-unstyled pl-0 ml-1 in collapse @if ($delivery_bias) show @endif" id="biasAdvanced">
							<li>
								<label class="custom-control custom-radio mb-0">
									<input type="radio" name="delivery_bias_delete" value="1" class="custom-control-input"
									       @if ($delivery_bias_delete) CHECKED @endif />
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">
										Delete it
									</span>
								</label>
							</li>
							<li>
								<label class="custom-control custom-radio mb-0">
									<input type="radio" name="delivery_bias_delete" value="0" class="custom-control-input"
									       @if (!$delivery_bias_delete) CHECKED @endif />
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">
										Move it to my <a href="{{ HTML_Kit::page_url() }}/tip/delivery-bias-purge"
											class="popover-trigger" id="delivery-bias-purge">defined mailbox</a>
										of
										<input type="text" class="form-control" name="delivery_bias_mailbox"
									       class="form-control"
									       value="{{  $delivery_bias_mailbox }}" size=10/>
									</span>
								</label>
							</li>
						</ul>

					</li>
				</ul>
			</li>
		</ol>
	</div>
</div>