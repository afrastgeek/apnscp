<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

namespace apps\saconfig\models;

class Tips
{
	public function get(string $index): ?array
	{
		$tip = array();
		$tip['demo'] = array(
			'body'  => 'This is an example help pop-up that will appear whenever you see a dotted link on the page.  ' .
				'If you are unsure of what a particular term means, just hover your mouse over it for further ' .
				'information.',
			'title' => 'Example Help'
		);
		$tip['score'] = array(
			'body'  => 'The score is an arbitrary value SpamAssassin gives to e-mails.  Certain attributes in an e-mail can impact a score on an ' .
				'e-mail by increasing or decreasing its score.  Should an e-mail score higher than the threshold, it will be tagged as spam.' .
				'<em>5</em> is the default value.  Occasionally, legitimate e-mails may be marked that high, especially if your Bayesian' .
				' database is new and you have opted to utilize Bayesian filtering.  If you would like to be conservative, a setting of ' .
				'<em>8.0</em> to <em>10.0</em> should be sufficient in minimizing false-positives.  A setting of <em>99</em> or higher disables it.',
			'title' => 'SpamAssassin Scoring'
		);
		$tip['tag'] = array(
			'body'  => 'An e-mail that is <em>tagged</em> means the subject of the e-mail changes from &quot;Hey there, trying out this e-mail&quot; to' .
				' &quot;[SPAM] Hey there, trying out this e-mail&quot; if it is marked as spam by SpamAssassin.',
			'title' => 'What is a tag?'
		);
		$tip['user-defined-tag'] = array(
			'body'  => 'You can create your own prefix for the subject of an e-mail.  Accepted input would be any character ' .
				'a-z, !@#$%^&*()-[]_+, and 0-9.  Additionally, you may use two pre-defined constants <em>_SCORE_</em>, ' .
				'which is the score SpamAssassin calculates for the e-mail and <em>_REQD_</em>, which is the required ' .
				'minimum score for an e-mail to be marked as spam.',
			'title' => 'User-defined Tags'
		);
		$tip['pop3'] = array(
			'body'  => 'Most e-mail accounts setup by the common user these days are POP3 accounts.  If you are unsure of how you setup your account,' .
				' then select this inbox type.  A good way to distinguish if your account is a POP3 is: are your e-mails deleted from the server ' .
				'once you download them or do you have <strong>only</strong> one mailbox for your mail account named &quot;Inbox&quot;?  ' .
				'If so, then it is probably a POP3 account',
			'title' => 'POP3'
		);
		$tip['imap'] = array(
			'body'  => 'IMAP accounts allow synchronization between multiple e-mail clients.  Chances are, you didn\'t need to put your mouse over this ' .
				'help blurb to find out if it is an IMAP account.  If you did, then a good way of finding out if your e-mail account ' .
				'is an IMAP is to check for more than one mailbox for your account.  Do you have separate folders named ' .
				'&quot;Inbox&quot;, &quot;Sent&quot;, &quot;Trash&quot;, and so on?  If so, it is probably an IMAP account.',
			'title' => 'IMAP'
		);
		$tip['delivery-bias-purge'] = array(
			'body'  => 'When setting a custom mailbox to move the e-mail to, if you use <em>_DELETE_</em>, then the e-mail will be deleted, ' .
				'instead of moved to a custom mailbox.',
			'title' => 'Special-Case Handling'
		);
		$tip['preview-safe'] = array(
			'body'  => 'By default, SpamAssassin will display the e-mail as it normally comes; however if you enable safe preview ' .
				', then SpamAssassin will make the original e-mail an attachment, preventing your e-mail client from opening up ' .
				'a potentially dangerous e-mail -- through scripting.  If your e-mail client is having difficulties viewing the ' .
				'e-mail normally, then select the text/plain MIME type instead of message/rfc822.',
			'title' => 'Safe Previews'
		);
		$tip['bayesian'] = array(
			'body'  => 'Bayesian filtering, summarized in the simplest sense, means that e-mails are analyzed for special strings that occur ' .
				'in spams and hams (legitimate e-mails).  SpamAssassin is able to analyze these occurrences and help decide whether an e-mail ' .
				'is spam or ham through scoring based upon the Bayesian probability (percentage).  A Bayesian percentage between (40,100]% will ' .
				'add points to an e-mails spam score, below 40% will reduce the score.  When you are first starting off and training it, most e-mails ' .
				'will get a 50% rating (+ 0.001 points).  As time progresses, SpamAssassin will learn to adapt and begin marking spams more ' .
				'confidently with higher ratings.  A properly trained system will usually mark spam with between (80,100]%',
			'title' => 'Bayesian Filtering Information'
		);
		$tip['razor'] = array(
			'body'  => 'Razor is a tool that checks subjects against a database of commonly used spam subjects.  If found, Razor will add between [0.056,1.485] points ' .
				'if there is a possibility the subject is a known spam subject (based upon probability and variability in the subject) and a score between ' .
				'[0.150,1.511] if the basic subject mark-up is in Razor\'s database of known spam subjects.',
			'title' => 'Razor'
		);
		$tip['pyzor'] = array(
			'body'  => 'Pyzor is a tool that checks subjects against a database of commonly used spam subjects.  If found, Pyzor will add between [0.056,1.485] points ' .
				'if there is a possibility the subject is a known spam subject (based upon probability and variability in the subject) and a score between ' .
				'[0.150,1.511] if the basic subject mark-up is in Pyzor\'s database of known spam subjects.',
			'title' => 'Pyzor'
		);
		$tip['bayesian-rules'] = array(
			'body'  => 'Enabling this option will enable the Bayesian classifier to add points to e-mails based upon spam probabilities.  Also see the ' .
				'&quot;Bayesian&quot; tooltip.',
			'title' => 'Bayesian Classifier Points'
		);
		$tip['auto-learn'] = array(
			'body'  => 'Auto-learn will attempt to automatically learn spam and hams as they come in based upon option three below.  Disabling this option also disables ' .
				'option number three.  You can auto-learn e-mails and not enable the Bayesian scoring facility of SpamAssassin if you disable option number four.',
			'title' => 'Bayesian Auto-learn'
		);


		$tip['maildrop'] = array('body' => 'If you use Hostineer for hosting, then you should select this option.  Otherwise if your home directory has a file named .mailfilter, then you are using Maildrop as a LDA.');
		$tip['maildir'] = array('body' => 'Maildir is the default mail storage used in fourth generation servers and on.  Maildir mailboxes are prefixed by a . and each sub-folder is delimited by a period (.).  For example, Mail/Spam/ is represented as .Mail.Spam on the filesystem.');
		$tip['mbox'] = array('body' => 'mbox is a legacy format seldom used by any provider. Messages are stored in a single text file.  Sub-folders are represented on the filesystem as directories (compare to Maildir).');

		return $tip[$index] ?? null;
	}
}
