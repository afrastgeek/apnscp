<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
	 */

	namespace Daphnie\Metrics;

	use Daphnie\Contracts\MetricProvider;
	use Daphnie\Metric;
	use Daphnie\MutableMetricTrait;

	class Mysql extends Metric implements MetricProvider
	{
		use MutableMetricTrait;

		public const ATTRVAL_MAP = [
			'conn'     => 'connections',
			'max_conn' => 'max_used_connections',
			'files'    => 'opened_files',
			'tables'   => 'opened_tables',
			'tchit'    => 'table_open_cache_hits',
			'tcmiss'   => 'table_open_cache_misses',
			'uptime'   => 'uptime',
			'q'        => 'queries',
			'questions'=> 'questions'
		];

		private const ATTR_BINDINGS = [
			'conn'  => [
				'label' => 'Total connections',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'max_conn' => [
				'label' => 'Max concurrent connections',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'files' => [
				'label' => 'Open files',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'tables' => [
				'label' => 'Open tables',
				'type'  => MetricProvider::TYPE_VALUE
			],
			'tchit' => [
				'label' => 'Table cache hits',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'tcmiss' => [
				'label' => 'Table cache misses',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'uptime'    => [
				'label' => 'Uptime',
				'unit'  => 'sec',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'q'         => [
				'label' => 'Queries',
				'type'  => MetricProvider::TYPE_MONOTONIC
			],
			'questions' => [
				'label' => 'Questions',
				'type'  => MetricProvider::TYPE_MONOTONIC
			]
		];
	}
