<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);

	/**
	 * Expand or reduce units
	 */
	class Formatter
	{

		/**
		 * @param float|int  $size      size in bytes
		 * @param int        $placespec unit or movement
		 * @param string|int $limit     optionally limit n places or unit
		 * @return null|string
		 */
		public static function reduceBytes($size, $placespec = 2, $limit = -1): ?string
		{
			$suffix = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
			if (!is_int($limit)) {
				$tmp = array_search(strtoupper($limit), $suffix, true);
				if (false === $tmp) {
					error("unknown unit `%s'", $limit);

					return null;
				}
				$limit = $tmp;
			}
			$chk = ($limit > -1);
			$i = 0;
			$a = $size;
			while ($size > 1024) {
				if ($chk && ($limit - $i) < 1) {
					break;
				}
				$size /= 1024;
				$i++;
			}

			return sprintf('%.' . $placespec . 'f %s', $size, $suffix[$i]);
		}

		/**
		 * Convert suffixed size to new target size
		 *
		 * @param string|int $size   suffixed string or bytes
		 * @param string     $target
		 * @param string     $native base units, empty to autodetect from $size
		 * @return null|float
		 */
		public static function changeBytes($size, $target = 'B', $native = null): ?float
		{
			if ($target === $native) {
				return (float)$size;
			}

			$target = strtoupper($target);
			$suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

			$num = (float)$size;
			$unit = 'B';
			$srcsfx = strtoupper((string)$native);
			if (!$native) {
				if (false === strpos((string)$size, '.')) {
					$srcsfx = strtoupper(trim((string)substr((string)$size, strlen((string)$num))));
				} else {
					$srcsfx = strtoupper(substr((string)$size, strspn($size, '0123456789.')));
				}
			}

			if ($srcsfx) {
				$unit = array_first($suffixes, static function ($suffix) use ($srcsfx) {
					if ($suffix === $srcsfx || $suffix[0] === $srcsfx) {
						return true;
					}
					return false;
				});

				if ($srcsfx === 'BYTES') {
					// literal usage of the term
					$unit = 'B';
				}
			}
			if ($target[-1] !== 'B') {
				$target .= 'B';
			}
			$pos1 = array_search($unit, $suffixes, true);
			$pos2 = array_search($target, $suffixes, true);
			if (false === $pos1 || false === $pos2) {
				error("invalid size constraits, `%s' -> `%s'", $unit, $target);
				return null;
			}
			$magnitude = $pos1 - $pos2;

			return $num * (1024 ** $magnitude);
		}

		/**
		 * Commafy a number
		 *
		 * Observes locale
		 *
		 * @param string $what
		 * @return string
		 */
		public static function commafy(string $what): string
		{
			$locales = localeconv();

			return preg_replace("/(\d)(?=(\d\d\d)+(?!\d))/",
				'$1' . $locales['mon_thousands_sep'],
				$what
			);
		}

		/**
		 * Convert duration into time
		 *
		 * @param int  $duration
		 * @param bool $radix     return numbers binned
		 * @return string|array
		 */
		public static function time(int $duration, bool $radix = false)
		{
			$units = [
				[30, 'month'],
				[24, 'day'],
				[60, 'hour'],
				[60, 'min']
			];
			$dechunkifier = 30 * 24 * 60 * 60;
			$buckets = [];
			$str = '';
			for ($i = 0, $n = count($units); $i < $n; $i++) {
				$cWholeUnit = (int)($duration / $dechunkifier);
				if (!$radix && $cWholeUnit) {
					$str .= $cWholeUnit . ' ' . \Illuminate\Support\Str::plural($units[$i][1], $cWholeUnit) . ' ';
				}
				$duration %= $dechunkifier;
				$dechunkifier /= $units[$i][0];
			}
			if ($radix) {
				return $buckets;
			}
			return $radix ? $buckets : rtrim($str, ' ');
		}
	}