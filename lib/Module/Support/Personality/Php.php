<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support\Personality;

	class Php extends Helpers\Base
	{

		const PRIORITY = 2;
		const NAME = 'php';

		protected $_tokens = array(
			'php_value' => array(
				'help' => 'Set a non-binary PHP option.'
			),
			'php_flag'  => array(
				'help' => 'Set a binary PHP option, on or off.'
			)
		);

		public function resolves($token)
		{
			return $token === 'php_value' || $token === 'php_flag';
		}

		public function test($directive, $val = null)
		{
			if ($directive == 'php_flag') {
				if (false === strpos($val, ' ')) {
					return error("php_flag requires format: 'name value'");
				}
				list($opt, $val) = explode(' ', $val);
				if ($val !== 'on' && $val !== 'off') {
					return error("option `%s' only accepts on or off value", $opt);
				}
			}
		}
	}