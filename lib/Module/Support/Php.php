<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support;

	use Module_Skeleton;

	/**
	 * Auxiliary support components for php module
	 *
	 * Class Module_Support_Php
	 *
	 * @see Php_Module
	 */
	abstract class Php extends Module_Skeleton
	{

		protected function getPersonalities()
		{
			$personalities = glob($this->personalityRoot() . '/virtual-*', GLOB_ONLYDIR);

			return array_map(
				static function ($f) {
					return substr(basename($f), \strlen('virtual-'));
				}, $personalities
			);
		}

		protected function personalityRoot()
		{
			return $this->web_config_dir();
		}

		protected function personalityExists($personality)
		{
			$path = $this->getPersonalityPathFromPersonality($personality);

			return file_exists($path);
		}

		protected function getPersonalityPathFromPersonality($personality, $config = '')
		{
			if ($config) {
				return \Opcenter\Http\Apache::siteConfigurationPath($config, $personality);
			}

			return \Opcenter\Http\Apache\Personality::getPersonalityPath($personality);
		}

		protected function getPersonalityPort($personality)
		{
			$path = $this->getPersonalityPathFromPersonality($personality);

			return trim(file_get_contents($path));
		}
	}
