<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Provides tar compression/decompression routines in the file manager
	 *
	 * @package Compression
	 */
	class Tar_Filter extends Archive_Base
	{
		protected static function extract_files_pipe($process, $destination, array $files = null, array $opts = null)
		{
			//$process->setEncoding('binary');
			return self::extract_files($process,
				$destination,
				$files,
				array('pipe' => 1/*$process->start()*/));
		}

		public static function extract_files($archive, $destination, array $file = null, array $opts = null)
		{
			if (isset($opts['pipe']) && false !== strpos($archive, '.gz')) {
				$cmd = 'tar -xzvf %s -C %s';
			} else {
				if (isset($opts['pipe']) && false !== strpos($archive, '.bz')) {
					$cmd = 'tar -xjvf %s -C %s';
				} else {
					$cmd = 'tar -xvf %s -C %s';
				}
			}
			$proc = parent::exec(
				$cmd,
				$archive,
				$destination,
				array(0),
				array('run' => 1/* @XXX pipe broken for now */)
			);
			if (0 & isset($opts['pipe'])) {
				$proc->pipeProg($opts['pipe']);
			}

			return $proc;
		}

		protected static function list_files_pipe($process, array $opts = null)
		{

			/*$proc = self::$fc->proc_broker (
				"echo 'Hello World!'",
				array(0),
				array('run' => 0)
			);
			$proc2 = self::$fc->proc_broker(
				'(cat - /tmp/test ; sleep 5 ;echo "hooray!")',
				array(0),
				array('run' => 0)
			);
			$proc2->pipeProg($proc->start());
			$proc2->setOption('run',1)->setOption('binary',1);
			var_dump($proc2->run());*/
			//$process->setEncoding('binary');
			return self::list_files($process, array('pipe' => 1/*$process->start()*/));
		}

		public static function list_files($archive, array $opts = null)
		{
			$tar = false;
			if (isset($opts['pipe']) && false !== strpos($archive, '.gz') || false !== strpos($archive, '.tgz')) {
				$cmd = 'tar -tzvf %s';
			} else {
				if (isset($opts['pipe']) && false !== strpos($archive, '.bz') || false !== strpos($archive, '.tbz')) {
					$cmd = 'tar -tjvf %s';
				} else {
					$cmd = 'tar -tvf %s';
				}
			}
			$proc = parent::exec(
				$cmd,
				$archive,
				array(0),
				array('run' => 1));


			if (0 & isset($opts['pipe'])) {
				$proc->pipeProg($opts['pipe']);
			}
			$proc = explode("\n", $proc['output']);
			$files = array();

			foreach ($proc as $line) {
				if (!preg_match(Regex::FILE_HDR_TAR, $line, $entry)) {
					continue;
				}
				//list ($null, $file_name, $size, $packed_size, $date, $attr, $crc) = $matches;
				$file_type = $entry['permissions'][0] == 'd' ? 'dir' : 'file';
				$size = $file_type == 'dir' ? 4096 : $entry['size'];

				$files[] = array(
					'file_name'   => $entry['name'],
					'file_type'   => $file_type,
					'can_read'    => true,
					'can_write'   => true,
					'can_execute' => true,
					'size'        => $size,
					'packed_size' => $size,
					'crc'         => '?',
					'link'        => 0,
					'date'        => strtotime($entry['ts'])
				);
			}

			return $files;

		}

	}

?>
