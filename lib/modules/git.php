<?php declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	/**
	 * Git management
	 *
	 * @package core
	 */
	class Git_Module extends Module_Skeleton
	{
		protected $exportedFunctions = ['*' => PRIVILEGE_SITE | PRIVILEGE_USER];

		/**
		 * Clone a repositroy
		 *
		 * @param string $repo
		 * @param string $target
		 * @param array  $opts
		 * @return bool
		 */
		public function clone(string $repo, string $target, array $opts): bool
		{
			$opts = array_key_map(static function ($k, $v) {
				$rhand = '';
				if ($v !== null) {
					$rhand = '=' . escapeshellarg((string)$v);
				}

				return (isset($k[1]) ? '--' : '-') . escapeshellarg($k) . $rhand;
			}, $opts);
			$ret = $this->pman_run('git clone ' . implode(' ', $opts) . ' %(repo)s %(target)s',
				[
					'repo'   => $repo,
					'target' => $target
				]
			);

			return $ret['success'] ?: error($ret['stderr']);
		}

		/**
		 * Clean repository of stray files
		 *
		 * @param string     $path
		 * @param bool       $dry
		 * @param bool       $dir remove directory as well
		 * @return bool|array
		 */
		public function clean(string $path, bool $dir = true, bool $dry = false)
		{
			$ret = $this->pman_run('cd %(path)s && git clean -f %(dry)s %(dir)s',
				[
					'path' => $path,
					'dry'  => $dry ? '-n' : '-q',
					'dir'  => $dir ? '-d' : null
				]
			);
			if (!$ret['success']) {
				error('Failed to clean repo: %s', $ret['stderr']);
			}
			if ($dry) {
				$lines = rtrim($ret['stdout']);
				if (!$lines) {
					return [];
				}
				return array_map(static function ($line) {
					if (0 === strpos($line, "Would remove ")) {
						return substr($line, 13);
					}

					return $line;
				}, explode("\n", $lines));
			}
			return $ret['success'];
		}

		/**
		 * Path is valid git repository
		 *
		 * @param string $path
		 * @return bool
		 */
		public function valid(string $path): bool
		{
			if (!IS_CLI) {
				return $this->query('git_valid', $path);
			}

			return file_exists($this->domain_fs_path($path . '/.git/HEAD'));
		}

		/**
		 * Stash pending changes into new commit
		 *
		 * @param string      $path
		 * @param string|null $message
		 * @return string|null
		 */
		public function stash(string $path, string $message = null): ?string
		{
			$ret = $this->pman_run('cd %(path)s && git stash save -q %(message)s',
				['path' => $path, 'message' => $message]);
			if (!$ret['success']) {
				error('Failed to stash repo: %s', $ret['stderr']);
			}
			return (string)$this->file_get_file_contents("${path}/.git/stash");
		}

		/**
		 * Reset repository to commit
		 *
		 * @param string      $path
		 * @param string|null $commit
		 * @param bool        $hard
		 * @return bool
		 */
		public function reset(string $path, ?string $commit = null, bool $hard = true): bool
		{
			if ($commit && !ctype_xdigit($commit)) {
				return error("Invalid commit `%s'", $commit);
			}
			$ret = $this->pman_run('cd %(path)s && git reset -q %(hard)s %(commit)s',
				['path' => $path, 'hard' => $hard ? '--hard' : '--mixed', 'commit' => $commit]);

			return $ret['success'] ?: error('Failed to reset repo: %s', $ret['stderr']);
		}

		/**
		 * List tags for repository
		 *
		 * @param string $path
		 * @return array|null
		 */
		public function tag(string $path): ?array
		{
			$ret = $this->pman_run('cd %(path)s && git tag', ['path' => $path]);
			if (!$ret['success']) {
				error('Failed to enumerate tags: %s', $ret['stderr']);

				return null;
			}

			return explode("\n", rtrim($ret['stdout']));
		}

		/**
		 * Initialize a git repository
		 *
		 * @param string $path
		 * @param bool   $bare
		 * @return bool
		 */
		public function init(string $path, bool $bare = true): bool
		{
			$ret = $this->pman_run('git init %(bare)s %(path)s',
				[
					'bare' => $bare ? '--bare' : null,
					'path' => $path
				]);

			if (!$ret['success']) {
				return error($ret['stderr']);
			}
			$ret = $this->pman_run(
				'cd %(path)s && git config user.email "%(email)s" && git config user.name "%(name)s"',
				[
					'path' => $path,
					'email' => $this->common_get_email(),
					'name'  => array_get($this->user_getpwnam(), 'gecos') ?: PANEL_BRAND . ' commit bot'
				]
			);
			return $ret['success'] ?: error($ret['stderr']);
		}

		/**
		 * Download objects and refs from another repository
		 *
		 * @param string $path
		 * @param array  $opts
		 * @return bool
		 */
		public function fetch(string $path, array $opts = []): bool
		{
			$opts = implode(' ', array_key_map(static function ($k, $v) {
				$k = (isset($k[1]) ? '--' : '-') . escapeshellarg($k);
				if (null === $v) {
					return $k;
				}

				return $k . '=' . escapeshellarg($v);
			}, $opts));
			$ret = $this->pman_run('cd %(path)s && git fetch ' . $opts, ['path' => $path]);

			return $ret['success'] ?: error('Failed to fetch: %s', $ret['stderr']);
		}

		/**
		 * Add files to an existing repo
		 *
		 * @param string     $path  repo path
		 * @param array|null $files files relative to repo; files are not escaped
		 * @return bool
		 */
		public function add(string $path, ?array $files = []): bool
		{
			$fileStr = $files === null ? '-A --ignore-errors' : implode(' ', array_map('escapeshellarg', $files));
			$ret = $this->pman_run('cd %(path)s && git add ' . $fileStr, [
				'path' => $path,
			], ['LANGUAGE' => 'en_US']);
			if ($files === null && !$ret['success'] && false !== strpos($ret['stderr'], 'Permission denied')) {
				return warn('Failed to add files: %s', $ret['stderr']);
			}
			return $ret['success'] ?: error('Failed to add files: %s', $ret['stderr']);
		}

		/**
		 * Get head commit
		 *
		 * @param string $path
		 * @return string|null|bool
		 */
		public function head(string $path)
		{
			if (!$this->valid($path)) {
				return null;
			}

			$ret = $this->pman_run('cd %(path)s && git rev-parse HEAD', ['path' => $path]);
			if (!$ret['success']) {
				error("Failed to fetch HEAD in `%s': %s", $path, $ret['stderr']);
				return false;
			}

			return rtrim($ret['stdout']);
		}

		/**
		 * Add files to ignore
		 *
		 * @param string $path
		 * @param        $files
		 * @return bool
		 */
		public function add_ignore(string $path, $files): bool
		{
			if (!$this->valid($path)) {
				return false;
			}

			$entries = $this->list_ignored_files($path);
			$gitPath = "${path}/.gitignore";

			foreach ($files as $line) {
				if (in_array((string)$line, $entries, true)) {
					warn('%(line)s already listed in %(path)s', ['line' => $line, 'path' => $gitPath]);
					continue;
				}
				$entries[] = $line;
			}

			return $this->file_put_file_contents($gitPath, implode("\n", $entries));
		}

		/**
		 * List ignored files
		 *
		 * @param string $path
		 * @return array
		 * @throws FileError
		 */
		public function list_ignored_files(string $path): array
		{
			$gitPath = "${path}/.gitignore";
			if (!$this->valid($path) || !$this->file_exists($gitPath)) {
				return [];
			}

			return preg_split('/\R+/m', $this->file_get_file_contents($gitPath));
		}

		/**
		 * Get last n commits
		 *
		 * @param string   $path
		 * @param int|null $max
		 * @return array
		 */
		public function list_commits(string $path, ?int $max = 5): array
		{
			if (!$this->valid($path)) {
				return [];
			}

			if ( $max !== null && ($max < 0 || $max > 999999) ) {
				error('Commit limit out of range');
				return [];
			}
			//git log -n 15 --oneline --format="%h %H %ct %s"
			$ret = $this->pman_run("cd %(path)s && git log %(hasFlag)s %(max)d --format='%%h %%H %%ct %%s'",
				['path' => $path, 'hasFlag' => null !== $max ? '-n' : '', 'max' => $max]);
			if (!$ret['success']) {
				error('Failed to run git log: %s', $ret['stderr']);
			}
			$commits = [];
			$hash = strtok($ret['stdout'], ' ');
			while ($hash) {
				$commits[$hash] = [
					'hash'    => strtok(' '),
					'ts'      => (int)strtok(' '),
					'subject' => strtok("\n")
				];
				$hash = strtok(' ');
			}

			return $commits;
		}

		/**
		 * Commit staged transaction
		 *
		 * @param string $path
		 * @param string $msg
		 * @return string|null commit hash or null
		 */
		public function commit(string $path, string $msg): ?string
		{
			$ret = $this->pman_run('cd %(path)s && git commit -qm %(msg)s && git rev-parse HEAD', [
				'path'  => $path,
				'msg'   => $msg
			]);

			if (!$ret['success']) {
				if (false !== strpos($ret['stdout'], 'nothing to commit')) {
					warn('No changes to save');
					return null;
				}
				error('Failed to commit: %s', coalesce($ret['stderr'], $ret['stdout']));
				return null;
			}

			return trim($ret['stdout']);
		}

		/**
		 * Checkout ref/tag
		 *
		 * @param string $path
		 * @param string|null $ref
		 * @param array|null $files optional files
		 * @return bool
		 */
		public function checkout(string $path, ?string $ref, array $files = null): bool
		{
			if ($files) {
				$files = implode(' ', array_map('escapeshellarg', $files));
			}
			$ret = $this->pman_run("cd %(path)s && git checkout %(ref)s $files", [
				'path' => $path,
				'ref'  => $ref,
			]);

			return $ret['success'] ?: error("Failed to checkout `%(ref)s': %(err)s",
				['ref' => $ref, 'err' => $ret['stderr']]);
		}

		/**
		 * git version
		 *
		 * @return string
		 */
		public function version(): string
		{
			$cache = \Cache_Global::spawn();
			$key = 'git.version';
			if (false === ($version = $cache->get($key))) {
				$ret = \Util_Process::exec('git --version');
				$version = rtrim(substr($ret['stdout'], strrpos($ret['stdout'],' ')+1));
				$cache->set($key, $version);
			}

			return $version;
		}
	}