<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;
	use Lararia\Jobs\Job;

	class VirusScanFinished extends Mailable
	{
		use SerializesModels;
		protected $job;

		/**
		 * Create a new message instance.
		 *
		 * @param Job\Report $report
		 */
		public function __construct(Job\Report $report)
		{
			$this->job = $report->current();
		}

		public function build()
		{
			$path = $this->job->getArguments()[0];
			$msg = 'Scan completed for path ' . $path . ' on ' .
				($this->job->web_get_hostname_from_docroot($path) ?? '<UNKNOWN>') . "\n\n" .
				'**Results**' . "\n" . '---' . "\n";

			if (!$this->job->hasErrors()) {
				$msg .= '```' . "\n" . $this->job->getReturn() . "\n" .
					'```';
			} else {
				foreach ($this->job->getLog() as $log) {
					$msg .= '* ' . $log['message'] . "\n";
				}
			}
			return $this->markdown('email.simple',
				[
					'job' => $this->job->hasErrors() ? $this->job : null,
					'msg' => $msg
				]
			)->subject('Scan completed')
				->to($this->job->common_get_email());
		}
	}