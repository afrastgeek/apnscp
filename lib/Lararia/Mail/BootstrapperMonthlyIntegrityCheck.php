<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Mail;


	class BootstrapperMonthlyIntegrityCheck extends BootstrapperFinished
	{
		public function build()
		{
			$subject = '[' . SERVER_NAME . '] system health report';
			return $this->markdown('email.admin.monthly-integrity-check',
				[
					'job' => $this->job,
					'ip' => \Opcenter\Net\Ip4::my_ip(),
					'hostname' => SERVER_NAME,
					'stats' => $this->getStats()
				]
			)->subject($subject)
				->to($this->job->common_get_email())
				->attach($this->job->getTee(), ['as' => 'bootstrapper-log.txt']);
		}
	}