<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	class MonthlyIntegrityJob extends BootstrapperTask
	{

		// only call on failure otherwise module handles
		const MAILABLE_TEMPLATE = \Lararia\Mail\BootstrapperMonthlyIntegrityCheck::class;
	}