<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs\Traits;

	use Illuminate\Support\Collection;
	use Lararia\Jobs\Contracts\GroupableInterface;
	use Laravel\Horizon\Contracts\JobRepository;
	use Laravel\Horizon\Contracts\TagRepository;
	use Laravel\Horizon\Events\JobPushed;
	use Laravel\Horizon\Repositories\RedisTagRepository;

	trait Groupable
	{

		/**
		 * @var string group identifier
		 */
		protected $group;

		protected $secondaryGroups = [];

		/**
		 * Get group tag
		 *
		 * @return null|string
		 */
		public function getGroupTag(): ?string
		{
			return $this->group;
		}

		/**
		 * Add group name to job tags
		 *
		 * @param JobPushed $event
		 * @return bool
		 */
		public function appendGroupToTag(JobPushed $event): bool
		{
			if (!$this->group) {
				return true;
			}
			$id = (int)$event->payload->id();
			$tags = array_unique(array_merge([$this->group], $event->payload->tags(), $this->secondaryGroups));
			$repo = resolve(TagRepository::class);
			$repo->add($id, $tags);

			return true;
		}

		/**
		 * Get jobs completed in group
		 *
		 * @return Collection
		 */
		public function getCompletedJobs(): Collection
		{
			return $this->filterJobs('completed');
		}

		/**
		 * Filter jobs in group for state
		 *
		 * @param string     $filter
		 * @param array|null $jobs
		 * @return Collection
		 */
		protected function filterJobs(string $filter, array $jobs = null): Collection
		{
			if (!\in_array($filter, ['pending', 'completed', 'failed', 'reserved'], true)) {
				fatal("unknown filter spec `%s'", $filter);
			}
			$members = $jobs ?? $this->getGroupMembers();
			$jobrepo = resolve(JobRepository::class);

			return $jobrepo->getJobs($members)->filter(static function ($item) use ($filter) {
				return $item->status === $filter;
			});
		}

		/**
		 * Get all job IDs belonging to group
		 *
		 * @return array
		 */
		public function getGroupMembers(): array
		{
			if (!$this->group) {
				return [];
			}
			$repo = resolve(TagRepository::class);

			return array_values((array)$repo->jobs($this->group));
		}

		public function getFailedJobs(): Collection
		{
			return $this->filterJobs('failed');
		}

		/**
		 * Group has pending jobs
		 *
		 * @return bool
		 */
		public function hasMore(string $tag = null): bool
		{
			if (!$tag) {
				return $this->getPendingJobs()->count() > 0 || $this->getReservedJobs()->count() > 0;
			}

			/** @var RedisTagRepository $repo */
			$repo = resolve(TagRepository::class);
			return \count($repo->jobs($tag)) > 0;
		}

		/**
		 * Get jobs waiting for execution in group
		 *
		 * @return Collection
		 */
		public function getPendingJobs(): Collection
		{
			return $this->filterJobs('pending');
		}

		public function getReservedJobs(): Collection
		{
			return $this->filterJobs('reserved');
		}

		/**
		 * Attach job to group tag
		 *
		 * @param string $group
		 * @return void
		 */
		public function addToGroup(string $group): void
		{
			if (!$this instanceof GroupableInterface) {
				fatal('class %(self)s must implement %(interface)s',
					['class' => static::class, 'inteface' => GroupableInterface::class]
				);
			}
			//debug("Adding job to group $group");
			$this->group = $group;
		}

		public function addSecondaryGroup(string $tag): void
		{
			if (!$this instanceof GroupableInterface) {
				fatal('class %(self)s must implement %(interface)s',
					['class' => static::class, 'inteface' => GroupableInterface::class]
				);
			}

			$this->secondaryGroups[] = $tag;
		}
	}