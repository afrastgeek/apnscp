<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	use Illuminate\Contracts\Queue\ShouldQueue;
	use Lararia\Jobs\Contracts\GroupableInterface;
	use Lararia\Jobs\Traits\Groupable;
	use Module\Support\Webapps\Notifier;
	use Module\Support\Webapps\UpdateCandidate;

	class UpdateAppJob extends Job implements ShouldQueue, GroupableInterface
	{
		use Groupable;
		const MAILABLE_TEMPLATE = \Lararia\Mail\WebappUpdateFinished::class;

		/**
		 * @var int number of attempts
		 */
		public $tries = 1;

		public $timeout = 240;
		/**
		 * @var UpdateCandidate candidate info
		 */
		protected $candidate;
		/**
		 * @var bool
		 */
		protected $status = false;

		/**
		 * Create a new job instance.
		 *
		 * @param UpdateCandidate $candidate
		 */
		public function __construct(UpdateCandidate $candidate)
		{
			$this->candidate = $candidate;
		}

		public function getCandidate(): UpdateCandidate
		{
			return $this->candidate;
		}

		/**
		 * Execute the job.
		 *
		 * @return bool
		 */
		public function fire()
		{
			$status = $this->candidate->process();
			if ($this->getCandidate()->getAssurance() && !$this->hasMore($this->getAssuranceTag())) {
				debug('Group finished - validating %s/%s', $this->candidate->getHostname(), $this->candidate->getPath());
				$this->candidate->validateAssurance();
			}

			return $status;
		}

		public function tags(): array
		{
			return [
				$this->candidate->getAppType(),
				$this->candidate->getVersion() . ' -> ' . $this->candidate->getNextVersion(),
				$this->candidate->getBaseUri()
			];
		}

		/**
		 * Get assurance tag
		 *
		 * @return string
		 */
		protected function getAssuranceTag(): string {
			return $this->candidate->getAssurance()->getTag();
		}

		public function getEmail(): ?string
		{
			$ctx = $this->candidate->getAuthContext();
			return $this->notificationContextWrapper($ctx);
		}

		public function getAdminCopyEmail(): ?string
		{
			$admin = \Auth::get_admin_login();
			$ctx = \Auth::context($admin);
			return $this->notificationContextWrapper($ctx);
		}

		private function notificationContextWrapper(\Auth_Info_User $ctx): ?string {
			$notifier = Notifier::instantiateContexted($ctx);
			// NB: "status" gets lost during jobification => processing => unserialization
			// it's never updated once sent to Redis. Use a separate approach, hasErrors(), to get status
			if (!$notifier->notify((bool)$this->getReturn())) {
				return null;
			}

			return $notifier->getEmail();
		}


	}