<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;
	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Binutils
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Postfix extends AlternativesTrigger
	{
		/**
		 * @var array
		 */
		protected $alternatives = [
			[
				'name'     => 'mta',
				'src'      => '/usr/sbin/sendmail',
				'dest'     => '/usr/sbin/sendmail.postfix',
				'priority' => 30
			],
			[
				'name'     => 'mta-rmail',
				'src'      => '/usr/bin/rmail',
				'dest'     => '/usr/bin/rmail.postfix',
				'priority' => 30
			],
			[
				'name'     => 'mta-sendmail',
				'src'      => '/usr/lib/sendmail',
				'dest'     => '/usr/lib/sendmail.postfix',
				'priority' => 30
			],
			[
				'name'     => 'mta-sendmailman',
				'src'      => '/usr/share/man/man8/sendmail.8.gz',
				'dest'     => '/usr/share/man/man1/sendmail.postfix.1.gz',
				'priority' => 30
			]
		];

		public function update(string $package): bool
		{
			Utils::reloadFilesystem();
			return parent::update($package);
		}

		public function install(string $package): bool
		{
			Utils::reloadFilesystem();
			return parent::install($package);
		}


	}