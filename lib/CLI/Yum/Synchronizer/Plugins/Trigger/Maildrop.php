<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Maildrop
	 *
	 * Abandon setuid perms for maildrop
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 */
	class Maildrop extends Trigger
	{
		const FILE_RELINK = '/usr/bin/maildrop';
		const SERVICE = 'siteinfo';

		public function install(string $package): bool
		{
			if (!file_exists(self::FILE_RELINK)) {
				return false;
			}
			$path = Utils::getServicePath(self::SERVICE) . self::FILE_RELINK;
			return copy(self::FILE_RELINK, $path) && chmod($path, 0755);
		}

		public function remove(string $package): bool
		{
			$path = Utils::getServicePath(self::SERVICE) . self::FILE_RELINK;
			if (file_exists($path)) {
				return unlink($path);
			}
			return true;
		}

		public function update(string $package): bool
		{
			return $this->remove($package) && $this->install($package);
		}


	}