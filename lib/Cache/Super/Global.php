<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Cache_Super_Global extends Cache_Base
	{
		const CACHE_PROXY = 'Cache_Super_Mproxy';

		public static function reset_prefix()
		{
			self::set_key('S:');
		}
	}