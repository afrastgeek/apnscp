<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */


	namespace Opcenter;

	class Versioning
	{
		/**
		 * Get next version from list
		 *
		 * @param array  $versions
		 * @param string $baseversion
		 * @param string $maximalbranch
		 * @return null|string
		 */
		public static function nextVersion(
			array $versions,
			string $baseversion,
			string $maximalbranch = '99999999.99999999.99999999'
		): ?string {
			if (!$versions) {
				return null;
			}
			\assert(version_compare($versions[0], $versions[\count($versions) - 1], '<='),
				'verify versions sorted in ascending order');
			$maximalbranch .= '.999999999';
			$old = [$baseversion];
			$baseversion .= '.0';
			do {
				$items = \count($versions);
				if ($items === 1) {
					break;
				} else if (!$items) {
					return $baseversion;
				}
				$midpoint = floor($items / 2);
				$right = \array_slice($versions, $midpoint);
				$left = \array_slice($versions, 0, $midpoint);
				$comp = version_compare($baseversion, $right[0], '>=');
				if ($comp && version_compare($maximalbranch, $right[0], '>=')) {
					$versions = $right;
				} else {
					$old = $right;
					$versions = $left;
				}
			} while ($midpoint);

			if (version_compare($maximalbranch, $old[0], '>=')) {
				return $old[0];
			}

			return substr($baseversion, 0, strrpos($baseversion, '.'));
		}

		/**
		 * Determine if version is most recent in distribution
		 *
		 * @param array       $versions   all releases
		 * @param string      $version    subject version
		 * @param string|null $branchcomp optional branch to limit search
		 * @return int 1 if current, 0 if not, -1 if dev or non-semantic branch
		 */
		public static function current(array $versions, string $version, string $branchcomp = null): int
		{
			if (!$versions) {
				return 1;
			}
			$latest = self::maxVersion($versions, $branchcomp);

			if (version_compare($version, $latest, '=')) {
				return 1;
			}
			if ($version && version_compare($version, $latest, '<')) {
				return 0;
			}

			return -1;
		}

		/**
		 * Get maximum version satisfied by $branchcomp
		 *
		 * @param array       $versions
		 * @param string|null $branchcomp limit
		 * @return null|string
		 */
		public static function maxVersion(array $versions, string $branchcomp = null): ?string
		{
			\assert(version_compare($versions[0], $versions[\count($versions) - 1], '<='),
				'verify versions sorted in ascending order');
			if (!$branchcomp) {
				return array_pop($versions);
			}
			// make sure branch is maximal
			$branchcomp .= '.999999999';
			if (version_compare(current($versions), $branchcomp, '>=')) {
				return null;
			}
			$comparator = static function ($version) use ($branchcomp) {
				if (!$version) {
					return 0;
				}

				return version_compare($version[0], $branchcomp, '>=') ? -1 : 1;
			};
			$vergroup = $versions;
			/**
			 * Examine midpoint to reduce comps and achieve logn
			 * Versions *must be sorted*
			 */
			do {
				if (\count($vergroup) <= 1) {
					$latest = $vergroup[0];
					break;
				}
				$index = ceil(\count($vergroup) / 2);
				$right = \array_slice($vergroup, $index);
				$result = $comparator($right);
				$latest = $vergroup[$index];
				if ($result === 1) {
					$vergroup = $right;
				} else if ($result === -1) {
					$vergroup = \array_slice($vergroup, 0, $index);
				}
			} while ($result !== 0);

			return $latest;
		}

		/**
		 * Cleanup version to major
		 *
		 * @param string $version
		 * @return string
		 */
		public static function asMajor(string $version): string
		{
			$version = self::asMinor($version);
			$rpos = (int)strrpos($version, '.');

			return !$rpos ? $version : (string)substr($version, 0, $rpos);
		}

		/**
		 * Cleanup version to major + minor level
		 *
		 * @param string $version
		 * @return string
		 */
		public static function asMinor(string $version): string
		{
			$version = self::asPatch($version);
			$rpos = (int)strrpos($version, '.');

			return !$rpos ? $version : (string)substr($version, 0, $rpos);
		}

		/**
		 * Cleanup version to patch level
		 *
		 * @param string $version
		 * @return string
		 */
		public static function asPatch(string $version): string
		{
			// @todo accept experimental builds?
			$parts = explode('.', $version);
			$size = \count($parts);
			$last = $parts[$size - 1];
			if (!ctype_digit($last)) {
				if ($last === 'x') {
					// implies entire branch
					$last = '9999999';
				} else if (false !== ($pos = strpos($last, '-'))) {
					$last = substr($last, 0, $pos);
				} else if ($size < 3) {
					fatal("unparseable version passed `%s'", $version);
				}
				$parts[$size - 1] = $last;
			}
			if ($size < 3) {
				for ($i = 3 - \count($parts); $i > 0; $i--) {
					$parts[] = '0';
				}
			} else if ($size > 3) {
				$parts = \array_slice($parts, 0, 3);
			}

			return implode('.', $parts);
		}

		/**
		 * Version is well-formed
		 *
		 * @param string $version
		 * @return bool
		 */
		public static function valid(string $version): bool
		{
			return is_scalar($version) && !strcspn($version, '.0123456789');
		}

		/**
		 * Get maximal version from semantic limiter
		 *
		 * @param string $version  current version
		 * @param array  $versions available versions
		 * @param string null|$limit    branch to limit
		 * @return string
		 */
		public static function nextSemanticVersion(string $version, array $versions, ?string $limit = 'patch'): string
		{
			if (!$limit || $limit === 'none') {
				// @TODO go 1.x => 2.0 => 2.x?
				return array_pop($versions);
			}

			if ($limit === 'major') {
				$branch = Versioning::asMajor($version);
			} else if ($limit === 'minor') {
				$branch = Versioning::asMinor($version);
			} else {
				$branch = Versioning::asPatch($version);
			}
			return Versioning::maxVersion($versions, $branch) ?? $version;
		}

		/**
		 * Compare $v1 to $v2 in canon
		 *
		 * @param string $v1
		 * @param string $v2
		 * @param string $operator
		 * @return bool
		 */
		public static function compare(string $v1, string $v2, string $operator = '='): bool
		{
			$av1 = explode('.', $v1);
			$av2 = explode('.', $v2);
			$places = max(\count($av1), \count($av2));
			for ($i = 1; $i < $places; $i++) {
				if (!isset($av1[$i])) {
					$av1[$i] = $av2[$i];
				} else if (!isset($av2[$i])) {
					$av2[$i] = $av1[$i];
				}
			}
			return version_compare(implode('.', $av1), implode('.', $av2), $operator);
		}

		/**
		 * Find minimum version satisfied by target
		 *
		 * @param string $target   version to satisfy
		 * @param array  $versions map min version => satisfied by
		 * @return string
		 */
		public static function satisfy(string $target, array $versions): ?string
		{
			$satisfier = null;
			foreach ($versions as $min => $version) {
				if (self::compare($target, (string)$min, '<')) {
					break;
				}

				$satisfier = (string)$version;
			}

			return $satisfier;
		}
	}