<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2017
	 */

	namespace Opcenter\Mail\Vacation\Providers\Mailbot\Options;

	use Opcenter\Mail\Vacation\GenericOption;

	class NoDupe extends GenericOption
	{
		const FLAG = '-d $HOME/.vacation.db';
		const DEFAULT = true;

		public function setValue($value): bool
		{
			return parent::setValue((bool)$value);
		}

		public function getFlag(): string
		{
			if ($this->value) {
				return parent::getFlag();
			}

			return '';
		}
	}
