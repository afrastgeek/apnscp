<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);

	namespace Opcenter;

	use Module\Provider;
	use Opcenter\Dns\Contracts\ServiceProvider;

	class Dns
	{
		use \NamespaceUtilitiesTrait;
		protected static $providers;
		protected static $registeredProviders = [];

		/**
		 * Requested provider exists
		 *
		 * @param string $provider
		 * @return bool
		 */
		public static function providerValid(string $provider): bool
		{
			return \in_array($provider, static::providers(), true) && (
				$provider === 'builtin' || class_exists(Provider::getClass(static::getProviderType(), $provider, 'Module'))
			);
		}

		/**
		 * Get list of registered providers
		 *
		 * @return array
		 */
		public static function providers(): array
		{
			if (static::$providers === null) {
				$class = static::getBaseClassName();
				$path = __DIR__ . '/' . $class . '/Providers/*';
				static::$providers = array_map(static function($x) {
					return strtolower(basename($x));
				}, glob($path, GLOB_ONLYDIR));
			}
			return static::$providers + append_config(array_keys(static::$registeredProviders));
		}

		public static function providerHasHelper(string $provider): bool
		{
			return class_exists(Provider::getClass(static::getProviderType(), $provider, 'Validator'));
		}

		/**
		 * Get default DNS provider
		 *
		 * @return string
		 */
		public static function default(): string
		{
			$c = strtoupper(static::getProviderType()) . '_PROVIDER_DEFAULT';
			$provider = \constant($c);
			// "null" can be set as null (sic) in config.ini
			return ($provider ?? 'null') ?: 'builtin';
		}

		/**
		 * Get provider helper
		 *
		 * @param string $provider
		 * @return ServiceProvider
		 */
		public static function getProviderHelper(string $provider): ServiceProvider
		{
			$c = Provider::getClass(static::getProviderType(), $provider, 'Validator');

			return new $c();
		}

		/**
		 * Get provider type
		 *
		 * @return string
		 */
		protected static function getProviderType(): string
		{
			return strtolower(static::getBaseClassName());
		}

		/**
		 * Register new DNS provider
		 *
		 * @param string $provider provider name
		 * @param string $module   path to module
		 */
		public static function registerProvider(string $provider, string $module): void
		{
			if (class_exists($module) && static::getBaseClassName($module) === 'Module') {
				$module = static::getNamespace($module);
			}
			static::$registeredProviders[$provider] = $module;
		}

		/**
		 * Get custom loader
		 *
		 * @param string $module
		 * @return string|null
		 */
		public static function customLoader(string $module): ?string
		{
			// @todo convert to namespace
			return static::$registeredProviders[$module] ?? null;
		}
	}