<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Service;

	use Opcenter\Filesystem;
	use Opcenter\SiteConfiguration;

	class Plans
	{
		const PLAN_PATH = OPCENTER_PLAN_PATH;

		/**
		 * List all plans
		 *
		 * @return array
		 */
		public static function list(): array
		{
			return array_filter(Filesystem::readdir(
				resource_path(static::PLAN_PATH), static function ($k) {
				if ($k[0] === '.') {
					return false;
				}

				return basename($k);
			}));
		}

		public static function default(): string
		{
			return OPCENTER_DEFAULT_PLAN;
		}

		/**
		 * Plan exists
		 *
		 * @param string $plan
		 * @return bool
		 */
		public static function exists(string $plan): bool
		{
			return file_exists(resource_path(static::PLAN_PATH) . "/${plan}");
		}

		public static function path(string $plan): ?string
		{
			return resource_path(self::PLAN_PATH . "/${plan}");
		}

		/**
		 * Assign plan default values
		 *
		 * @param string $plan
		 * @param array  $defaults
		 * @return bool
		 */
		public static function assignDefaults(string $plan, array $defaults): bool
		{
			if (!self::exists($plan)) {
				fatal("Plan `%s' does not exist", $plan);
			}

			$path = self::path($plan);
			foreach ($defaults as $svc => $params) {
				$svcpath = "${path}/${svc}";
				$built = \Util_Conf::build_ini($params);
				if (!file_put_contents($svcpath, '[DEFAULT]' . PHP_EOL . $built)) {
					return error("Failed to write configuration in `%s'", $svcpath);
				}
			}

			return true;
		}

		public static function remove(string $plan): bool
		{
			if (!self::exists($plan)) {
				return error("Plan `%s' does not exist", $plan);
			}
			$path = self::path($plan);
			if (!file_exists($path)) {
				return error("Path `%s' does not exist", $path);
			}
			// @todo check if any sites use it
			Filesystem::readdir($path, static function ($file) use ($path) {
				unlink("${path}/${file}");
			});
			return rmdir($path);
		}

		/**
		 * Return values in $plan1 that differ in $plan2
		 *
		 * @param string $plan1
		 * @param string $plan2
		 * @return array
		 */
		public static function diff(string $plan1, string $plan2): array
		{
			if (!static::exists($plan1)) {
				fatal("Plan `%s' does not exist", $plan1);
			}
			$reader = new SiteConfiguration(null);
			$reader->setPlanName($plan1);
			if (!static::exists($plan2)) {
				return $reader->getDefaultConfiguration();
			}
			$reader2 = new SiteConfiguration(null);
			$reader2->setPlanName($plan2);
			$diffs = [];
			foreach ($reader->getDefaultConfiguration() as $svc => $vars) {
				$sdiff = \Util_PHP::array_diff_assoc_recursive($vars, $reader2->getDefaultConfiguration($svc));
				if ($sdiff) {
					$diffs[$svc] = $sdiff;
				}
			}
			return $diffs;
		}
	}
