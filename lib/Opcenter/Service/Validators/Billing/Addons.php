<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Billing;

	use Opcenter\Service\ServiceValidator;

	class Addons extends ServiceValidator
	{
		const DESCRIPTION = 'Additional internal "invoices" to attach to account';
		const VALUE_RANGE = '<array>';

		public function valid(&$value): bool
		{
			if (!\is_array($value) && $value) {
				return error("addons must be empty or an array, `%s' given", $value);
			}

			return true;
		}
	}
