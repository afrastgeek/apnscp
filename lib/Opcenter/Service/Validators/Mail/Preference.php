<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mail;

	use Opcenter\Service\ServiceValidator;

	class Preference extends ServiceValidator
	{
		const DESCRIPTION = 'Default SMTP MX priority';
		const VALUE_RANGE = '[0-100]';

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'enabled')) {
				$value = null;

				return true;
			}
			if ($value === null) {
				$value = 10;
			}

			return \is_int($value) && $value >= 0;
		}

	}