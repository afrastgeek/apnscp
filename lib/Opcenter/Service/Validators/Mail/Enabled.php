<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mail;

	use Opcenter\Database\PostgreSQL;
	use Opcenter\Provisioning\Mail;
	use Opcenter\Provisioning\Pam;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall, AlwaysValidate, ServiceReconfiguration
	{
		use \FilesystemPathTrait;

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			if (!Mail::install($svc)) {
				return false;
			}
			(new Pam($svc))->enable('imap')->enable('pop3')->enable('smtp');
			$afi = $svc->getSiteFunctionInterceptor();
			if (\Error_Reporter::is_error()) {
				return false;
			}
			$afi->email_add_virtual_transport($svc->getServiceValue('siteinfo', 'domain'));
			if (\Error_Reporter::is_error()) {
				\Error_Reporter::downgrade(\Error_Reporter::E_WARNING);
			}

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$afi = $svc->getSiteFunctionInterceptor();
			foreach ($afi->email_list_virtual_transports() as $transport) {
				$waserr = \Error_Reporter::is_error();
				$afi->email_remove_virtual_transport($transport);
				if (!$waserr && \Error_Reporter::is_error()) {
					\Error_Reporter::downgrade(\Error_Reporter::E_WARNING);
				}
			}

			$pamh = new Pam($svc);
			foreach (['imap', 'pop3', 'smtp'] as $pamsvc) {
				if (!$pamh->exists($pamsvc)) {
					continue;
				}
				$pamh->disable($pamsvc);
				$pamh->terminate($pamsvc);
			}
			Mail::uninstall($svc);

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$olddomain = $this->ctx->getOldServiceValue('siteinfo', 'domain');
			$newdomain = $this->ctx->getNewServiceValue('siteinfo', 'domain');
			if ($olddomain !== $newdomain) {
				if (\in_array($olddomain, $this->ctx->getNewServiceValue('aliases', 'aliases'), true)) {
					return warn("Previous domain `%(domain)s' has been demoted to an alias. Not renaming email addresses for `%(domain)s'.",
						['domain' => $olddomain]
					);
				}
				if (\in_array($newdomain, $this->ctx->getOldServiceValue('aliases', 'aliases'), true)) {
					return warn("New domain `%(domain)s' has been promoted from an alias. Not renaming email addresses for `%(domain)s'.",
						['domain' => $newdomain]
					);
				}
				$dbh = \PostgreSQL::pdo();
				$query = PostgreSQL::vendor('mail')->renameDomain($olddomain, $newdomain);
				if (false === $dbh->exec($query)) {
					return error("failed to rename email domain from `%s' to `%s': %s",
						$olddomain, $newdomain, $dbh->errorInfo()[2]);
				}
			} else if (!$old && $new) {
				return $this->populate($svc);
			} else if ($old && !$new) {
				return $this->depopulate($svc);
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}


	}

