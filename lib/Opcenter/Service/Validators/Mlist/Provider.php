<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mlist;

	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\ServiceValidator;

	class Provider extends ServiceValidator implements DefaultNullable
	{
		const DESCRIPTION = 'Set mailing list provider';
		const VALUE_RANGE = '[majordomo]';

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'enabled')) {
				return true;
			}

			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}

			if (!\Opcenter\Mail::listValid($value)) {
				return error("Unknown mlist provider `%s'", $value);
			}

			return true;
		}

		public function getDefault()
		{
			return 'majordomo';
		}
	}