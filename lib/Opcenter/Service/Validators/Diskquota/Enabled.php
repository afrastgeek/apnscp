<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;


	/**
	 * Class Enabled
	 *
	 * @package Opcenter\Service\Validators\Siteinfo
	 *
	 * Critical account bootstrapping
	 */
	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements AlwaysValidate, ServiceInstall, ServiceReconfiguration
	{
		public const DESCRIPTION = 'Limit storage and file count';

		public function valid(&$value): bool
		{
			return parent::valid($value);
		}


		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$this->reconfigure(null, 1, $svc);

			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				// don't toss out quotas when service is disabled
				$dquota = $fquota = 0;
			} else {
				$dquota = (int)\Formatter::changeBytes($this->ctx->getNewServiceValue(null, 'quota') ?? 0, 'KB',
					$this->ctx->getNewServiceValue(null, 'units'));
				$fquota = $this->ctx->getNewServiceValue(null, 'fquota') ?? 0;
			}

			return \Opcenter\Filesystem\Quota::setGroup($svc->getServiceValue('siteinfo', 'admin'), $dquota, $fquota,
				(int)($dquota * 0.99), (int)($fquota * 0.99));
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return \Opcenter\Filesystem\Quota::setGroup($svc->getServiceValue('siteinfo', 'admin'), 0, 0, 0, 0);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure('', null, $svc);
		}
	}