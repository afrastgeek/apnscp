<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Spamfilter;

	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\ServiceValidator;

	class Provider extends ServiceValidator implements DefaultNullable
	{
		const DESCRIPTION = 'Inbound spam filter';
		// @todo hardcoded
		const VALUE_RANGE = '[spamassassin,rspamd]';

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'enabled')) {
				$value = null;
				warn('no spam filter provided');

				return true;
			}

			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}

			if (!$value && !MAIL_SPAM_FILTER) {
				return !MAIL_SPAM_FILTER ?: error("Spam filter may not be disabled on a site-by-site basis");
			}

			if (!\Opcenter\Mail::filterValid($value)) {
				return error("Unknown spamfilter provider `%s'", $value);
			}

			return true;
		}

		public function getDefault()
		{
			return MAIL_SPAM_FILTER;
		}


	}