<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\System\Cgroup\Attributes\Cpu\Shares;

	class Cpuweight extends Ioweight
	{
		const DESCRIPTION = 'Prioritize CPU to process tasks';

		public function valid(&$value): bool
		{
			if ($value === Shares::CPU_SHARES_DEFAULT) {
				info('Converting cgroup,cpuweight %d to %d', Shares::CPU_SHARES_DEFAULT, Shares::WEIGHT_DEFAULT);
				$value = Shares::WEIGHT_DEFAULT;
			}
			return parent::valid($value);
		}


	}
