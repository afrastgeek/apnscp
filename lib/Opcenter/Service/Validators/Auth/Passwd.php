<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Auth;

	use Opcenter\Auth\Password;
	use Opcenter\Auth\Shadow;

	class Passwd extends Common
	{
		const DESCRIPTION = 'Prompt interactively for password';

		public function valid(&$value): bool
		{
			if (!$value) {
				// tpasswd will generate a random password otherwise
				return true;
			}

			$this->clear('tpasswd', 'cpasswd');
			$this->ctx->set('cpasswd', Shadow::crypt($this->prompt()));

			return true;
		}

		public function write() {
			return null;
		}

		/**
		 * Prompt for password
		 *
		 * @return string
		 */
		private function prompt(): string
		{
			do {
				$pass = \Util_Terminal::password_interactive();
				if (Password::strong($pass)) {
					return $pass;
				}
			} while (true);
		}

		public function getDescription(): string
		{
			return static::DESCRIPTION;
		}
	}