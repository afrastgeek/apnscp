<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Auth;

	use Opcenter\Auth\Password;
	use Opcenter\Auth\Shadow;
	use Opcenter\Service\Contracts\SquashValue;

	class Tpasswd extends Common implements SquashValue
	{
		const DESCRIPTION = 'Plain-text password to use for admin user';

		public function valid(&$value): bool
		{
			if (!$value && $this->isSet('cpasswd')) {
				return true;
			} else if (!$value && !$this->isSet('cpasswd', 'passwd')) {
				if ($this->ctx->hasOld()) {
					return true;
				}
				$value = Password::generate(32);
				info("No password specified - generating random password: `%s'", $value);

			} else if (!Password::strong($value)) {
				return false;
			}
			$this->ctx->set('cpasswd', Shadow::crypt($value));
			$this->clear('passwd');

			return true;
		}

		public function write()
		{
			return null;
		}
	}