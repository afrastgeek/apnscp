<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Service\Validators\Rampart;

	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceReconfiguration
	{
		use \FilesystemPathTrait;

		public const DESCRIPTION = 'Delegate brute-force whitelisting';

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$ips = $this->ctx->getServiceValue(null, 'whitelist', []);
			return $this->reconfigure([], $ips, $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			$this->ctx['whitelist'] = [];
			$ips = $this->ctx->getOldServiceValue(null, 'whitelist', []);
			return $this->reconfigure($ips, [], $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old && !$new) {
				return $this->depopulate($svc);
			}
			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			// @TODO rollback needs to reenroll purged IPs
			return true;
		}
	}

