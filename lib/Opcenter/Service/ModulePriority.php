<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service;

	use MikeRoetgers\DependencyGraph\DependencyManager;
	use MikeRoetgers\DependencyGraph\GenericOperation;

	class ModulePriority
	{

		/**
		 * @var array elements to prioritize
		 */
		protected $items;
		/**
		 * @var array dependency list
		 */
		protected $dependencies;

		private function __construct(array $items, array $dependencies)
		{
			$this->items = $items;
			$this->dependencies = $dependencies;
		}

		/**
		 * Prioritize module configuration on dependency map
		 *
		 * @param array $names
		 * @param array $depmap
		 * @return array
		 */
		public static function prioritize(array $names, array $depmap): array
		{
			return (new static($names, $depmap))->sort();
		}

		/**
		 * Perform a naive sort on dependencies without thorough checking
		 *
		 * @todo check for cyclic references
		 * @return array
		 */
		public function sort(): array
		{
			$graph = $this->score();
			$map = [];
			while (!empty($dep = $graph->getExecutableOperations())) {
				foreach ((array)$dep as $op) {
					$map[] = $op->getId();
					$graph->markAsExecuted($op);

				}
			}

			return $map;
		}

		/**
		 * Score dependencies based upon claims
		 *
		 * @return array
		 */
		protected function score(): DependencyManager
		{
			$graph = new DependencyManager();
			$graphModules = array_build($this->items, static function ($key, $m) {
				return [$m, new GenericOperation($m)];
			});

			array_map(static function ($m) use ($graph, $graphModules) {
				$graph->addOperation($graphModules[$m]);
			}, array_keys($graphModules));

			foreach ($this->dependencies as $depName => $deps) {
				$moduleMap = $graphModules[$depName];
				foreach ($deps as $dep) {
					$graph->addDependencyByOperation($graphModules[$dep], $moduleMap);
				}
			}

			return $graph;
		}
	}