<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2018
	 */

	namespace Opcenter\Net;

	use PDO;

	class Fail2ban
	{
		const DATABASE = '/var/lib/fail2ban/fail2ban.sqlite3';

		/**
		 * Map actions to jails
		 *
		 *
		 * @return array|null jails or null on error
		 */
		public static function map(): ?array
		{
			$maps = [];
			if (null === ($jails = static::getJails())) {
				return null;
			}
			foreach ($jails as $jail) {
				$actions = [];
				foreach (static::getJailActions($jail) as $action) {
					$actions[$action] = static::getActionCommand($jail, $action);
				}
				$maps[$jail] = $actions;
			}

			return $maps;
		}

		/**
		 * Get configured jails
		 *
		 * @return array|null
		 */
		public static function getJails(): ?array
		{
			$ret = \Util_Process::exec('fail2ban-client status');
			if (!$ret['success']) {
				error("failed to query fail2ban, `%s'", $ret['stderr']);

				return null;
			}
			if (!preg_match('/Jail list:\s*(.*)$/m', $ret['stdout'], $matches)) {
				// no jails configured?
				return [];
			}
			$jails = preg_split('/\s*,\s*/', $matches[1], -1, PREG_SPLIT_NO_EMPTY);

			return $jails;
		}

		/**
		 * Get actions for jail
		 *
		 * @param string $jail jail name
		 * @return array
		 */
		public static function getJailActions(string $jail): array
		{
			$ret = \Util_Process_Safe::exec('fail2ban-client get %s actions', $jail);
			strtok($ret['stdout'], "\n");
			if (false === ($line = strtok("\n"))) {
				// no actions defined for jail?
				return [];
			}
			$actions = preg_split('/,\s*/', $line);
			return $actions;
		}

		/**
		 * Get action command
		 *
		 * @param string $jail   jail name
		 * @param string $action action name
		 * @return string
		 */
		public static function getActionCommand(string $jail, string $action): string
		{
			$ret = \Util_Process_Safe::exec('fail2ban-client get %s action %s actionban', $jail, $action);
			$resolved = static::resolve(trim($ret['stdout']), $jail, $action);

			return $resolved;
		}

		/**
		 * Expand variable placeholders in action
		 *
		 * @param string $command system command
		 * @param string $jail    jail name
		 * @param string $action  action name
		 * @return string
		 */
		private static function resolve(string $command, string $jail, string $action): string
		{
			return preg_replace_callback('/<((?!<)[^>]+)>/', static function (array $matches) use ($jail, $action) {
				$cmd = 'fail2ban-client get %(jail)s action %(action)s %(setting)s';
				if (\in_array($matches[1], ['ip', 'failures'])) {
					return '<RUNTIME VALUE <' . $matches[1] . '>>';
				}

				if (\in_array($matches[1], ['bantime', 'maxretry', 'maxmatches', 'actions', 'findtime'], true)) {
					// action is not compulsory
					$cmd = 'fail2ban-client get %(jail)s %(setting)s';
				}

				$ret = \Util_Process_Safe::exec($cmd,
					['jail' => $jail, 'action' => $action, 'setting' => $matches[1]]);
				if (!$ret['success']) {
					return '<' . $matches[1] . '>';
				}
				$resp = trim($ret['stdout']);
				if (false !== strpos($resp, '<')) {
					return static::resolve($resp, $jail, $action);
				}

				return $resp;
			}, $command);
		}

		public static function getDatabaseHandler(): \PDO
		{
			return new \PDO('sqlite:' . self::DATABASE);
		}

		/**
		 * Sum of bans between window
		 *
		 * @param int $begin
		 * @param int $end
		 * @return array
		 */
		public static function bansBetween(int $begin, int $end): array
		{
			$db = self::getDatabaseHandler();
			$rs = $db->query("SELECT jail, COUNT(*) AS count FROM bans WHERE timeofban >= $begin AND timeofban < $end  GROUP BY (jail)");
			$all = [];
			foreach ($rs->fetchAll(PDO::FETCH_ASSOC) as $rec) {
				$all[$rec['jail']] = (int)$rec['count'];
			}

			return $all;
		}
	}
