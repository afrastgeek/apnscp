<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Kernel implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!\in_array($val, $this->getValues(), true)) {
				return error('Unknown kernel setting');
			}

			if ($val === 'kernel-lt' && version_compare(OS_VERSION, '8.0', '>=')) {
				return error("kernel-lt package unavailable on CentOS 8+");
			}

			$cfg = new Config();
			$cfg['prefer_experimental_kernel'] = $val === 'system' ? false : true;
			if ($val !== 'system') {
				$cfg['custom_kernel_rpm'] = $val === 'stable' ? 'kernel-lt' : 'kernel-ml';
			}
			unset($cfg);
			Bootstrapper::run('system/kernel');

			return true;
		}

		public function get()
		{
			$cfg = new Config();
			if (!$cfg['prefer_experimental_kernel']) {
				return 'system';
			}

			return array_get($cfg, 'custom_kernel_rpm', 'kernel-lt') === 'kernel-lt' ? 'stable' : 'experimental';
		}

		public function getHelp(): string
		{
			return 'Change default kernel';
		}

		public function getValues()
		{
			return ['system', 'experimental', 'stable'];
		}

		public function getDefault()
		{
			return 'system';
		}

	}
