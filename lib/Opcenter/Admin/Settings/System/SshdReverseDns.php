<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SshdReverseDns implements SettingsInterface
	{
		const FILE = '/etc/sshd/sshd_config';

		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!\is_bool($val)) {
				warn("Value `%s' converted to boolean", $val);
			}

			$cfg = new Bootstrapper\Config();
			$cfg['sshd_use_dns'] = $val;
			$cfg->sync();

			Bootstrapper::run('system/sshd');

			return true;
		}

		public function get()
		{

			if (!file_exists(self::FILE)) {
				return true;
			}

			$directives = array_reverse(file(self::FILE, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES));
			foreach ($directives as $directive) {
				if (false !== stripos($directive, 'usedns ')) {
					return array_get(preg_split('/\s+/', $directive), '1', 'on') === 'on';
				}
			}

			return true;

		}

		public function getHelp(): string
		{
			return 'Reverse DNS of connecting clients';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
