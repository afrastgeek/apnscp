<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	class Bootstrapper implements SettingsInterface
	{
		public function get(...$var)
		{
			$cfg = new Config();

			if (!isset($var[0])) {
				return $cfg->toArray();
			}

			$var = $var[0];
			if (false === strpos($var, '/')) {
				return $cfg[$var] ?? null;
			}
			// loading role-specific defaults
			$defaults = $cfg->loadRole($var);

			return array_intersect_key($cfg->toArray(), $defaults) + $defaults;
		}

		/**
		 * Supports dot notation
		 *
		 * @param       $var
		 * @param mixed ...$val
		 * @return bool
		 */
		public function set($var, ...$val): bool
		{
			$cfg = new Config();
			if (!isset($val[0])) {
				return error("Missing value for configuration setting `%s'", $var);
			}
			$val = $val[0];
			if (!isset($cfg[$var])) {
				info("Setting variable `%s'. Previous not present in apnscp-vars-runtime.yml.", $var);
			}
			$cfg[$var] = $val;
			$cfg->sync();
			return true;
		}

		public function getHelp(): string
		{
			return 'Set Bootstrapper parameter';
		}

		public function getValues()
		{
			return 'mixed';
		}

		public function getDefault()
		{
			return 'mixed';
		}
	}