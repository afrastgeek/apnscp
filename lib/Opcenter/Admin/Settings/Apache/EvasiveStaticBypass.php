<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Apache;
	use Opcenter\Provisioning\ConfigurationWriter;
	use Tivie\HtaccessParser\Parser;
	use Tivie\HtaccessParser\Token\Directive;

	class EvasiveStaticBypass implements SettingsInterface
	{
		protected const CONFIG = '/etc/httpd/conf.d/evasive.conf';
		protected const MARKER = 'FILEMATCH';
		protected const TEMPLATE = 'rampart.evasive.static';

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}
			if (!\is_bool($val)) {
				return error("Unknown value `%s'", $val);
			}
			$contents = file_get_contents(self::CONFIG);
			if ($val) {
				$contents .= "\n" . '# BEGIN ' . static::MARKER . "\n" .
					$this->templateConfig() . "\n" .
					'# END ' . static::MARKER . "\n";
			} else {
				$contents = preg_replace('/^\s*# BEGIN ' . static::MARKER . '.*?(?<=END ' . static::MARKER . ')/ms', '', $contents);
			}

			file_put_contents(self::CONFIG, rtrim($contents), LOCK_EX);
			Apache::reload();

			return true;
		}

		private function templateConfig(): string
		{
			$writer = new ConfigurationWriter(static::TEMPLATE, null);
			return (string)$writer->compile();
		}

		public function get()
		{
			if (!file_exists(self::CONFIG)) {
				return false;
			}
			$contents = file_get_contents(self::CONFIG);
			return false !== strpos($contents, ' ' . static::MARKER);
		}

		public function getHelp(): string
		{
			return 'Ignore mod_evasive counters for static resources';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}