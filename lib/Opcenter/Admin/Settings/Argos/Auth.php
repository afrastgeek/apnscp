<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Argos;

	use Opcenter\Admin\Settings\SettingsInterface;

	class Auth implements SettingsInterface
	{

		/**
		 * Get authentication token
		 *
		 * @param array $var
		 * @return array
		 */

		public function get(...$var)
		{
			if (!isset($var[0])) {
				$var[0] = Backend::BACKEND_NAME;
			}
			if (null === ($instance = \Opcenter\Argos\Config::get())) {
				// not configured
				return null;
			}

			return $instance->backend(...$var)->getAuthentication();
		}

		/**
		 * Set Argos authentication
		 *
		 * Valid options: name, token, user
		 *
		 * @param string $val  backend
		 * @param array  $xtra extra options
		 * @return bool
		 */
		public function set($val = 'default', ...$xtra): bool
		{

			if (empty($xtra)) {
				return error("Authentication must specify backend and authentication or '' to apply authentication to all backends");
			}
			$instance = \Opcenter\Argos\Config::get();
			if (!$val) {
				if (!$instance) {
					return error("No backends are defined in `%s'", \Opcenter\Argos\Config::CONFIGURATION_FILE);
				}
				$val = $instance->getBackends();
			}

			foreach ((array)$val as $backend) {
				$backend = $instance->backend($backend);
				if (!$backend->setAuthentication(...$xtra)) {
					return error("Failed to set authentication for backend `%s'", $backend);
				}
			}

			return true;
		}

		public function getHelp(): string
		{
			return 'Set Argos backend credentials';
		}

		public function getValues()
		{
			return 'string|array';
		}

		public function getDefault()
		{
			return [];
		}
	}