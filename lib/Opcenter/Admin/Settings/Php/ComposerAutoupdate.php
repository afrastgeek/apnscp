<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Php;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\Setting;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Admin\Settings\System\IntegrityCheck;

	class ComposerAutoupdate implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			$cfg = new Config();
			if ($val && !(new IntegrityCheck())->get()) {
				warn("%s is disabled. This option will have no effect unless upcp -sb is manually ran.", Setting::scopeName(IntegrityCheck::class));
			}
			$cfg['composer_keep_updated'] = (bool)$val;
			if ($val) {
				info("Performing Composer update now");
				unset($cfg);
				Bootstrapper::run('php/composer');
			}
			return true;
		}

		public function get()
		{
			return (new Config())['composer_keep_updated'] ?? $this->getDefault();
		}

		public function getHelp(): string
		{
			return 'Update Composer during platform scrubs';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
