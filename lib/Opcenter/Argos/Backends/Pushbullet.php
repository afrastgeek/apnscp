<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Pushbullet extends Backend
	{
		protected $accessToken;
		protected $deviceIden;
		protected $email;

		public function getAuthentication()
		{
			return $this['access_token'];
		}

		public function setAuthentication(...$vars): bool
		{
			$this['access_token'] = $vars[0];

			return true;
		}

		public function setEmail($email): bool
		{
			if (!preg_match(\Regex::EMAIL, $email)) {
				return error("Invalid email address `%s'", $email);
			}
			$this->email = $email;

			return true;
		}
	}