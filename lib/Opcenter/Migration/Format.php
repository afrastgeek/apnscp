<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Migration;

	class Format
	{
		use \NamespaceUtilitiesTrait;

		/**
		 * Specified file is formatted of type
		 *
		 * @param string $type
		 * @param string $file
		 * @return bool
		 */
		public static function is(string $type, string $file): bool
		{
			$class = static::classFrom($type);
			/** @var BaseMigration */
			if ((new $class)->valid($file)) {
				return true;
			}
			return false;
		}

		/**
		 * Get class name from format
		 *
		 * @param string $format
		 * @return string
		 */
		public static function classFrom(string $format): string
		{
			if ($format === 'apnscp') {
				$format = 'apiscp';
			}
			return self::appendNamespace('Formats\\' . studly_case($format));
		}

		/**
		 * Requested format type valid for requested task
		 *
		 * @param string $format
		 * @param string $task
		 * @return bool
		 */
		public static function formatValidForTask(string $format, string $task): bool
		{
			if ($format === 'apnscp') {
				$format = 'apiscp';
			}
			$class = studly_case($task);
			return \in_array($format, \call_user_func([self::appendNamespace($class), 'getFormats']), true);
		}
	}