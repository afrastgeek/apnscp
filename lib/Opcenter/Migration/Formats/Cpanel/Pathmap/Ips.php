<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Filesystem;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Notes\NetInfo;
	use Opcenter\Migration\Notes\User;
	use Opcenter\SiteConfiguration;

	class Ips extends ConfigurationBuilder
	{
		public function parse(): bool
		{
			if (!file_exists($path = $this->getDirectoryEnumerator()->getPathname() . '/related_ips')) {
				return true;
			}
			$ips = file($path, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
			if (!$ips) {
				return true;
			}
			if (\count($ips) > 1) {
				warn('apnscp only supports 1 IPv6 at this time - ignoring %d additional IPs: %s',
					\count($ips)-1,
					implode(',', \array_slice($ips, 1))
				);
			}

			$note = $this->bill->getNote(NetInfo::class);
			$note->setIp6($ips[0]);

			return true;
		}
	}

