<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Notes;

	use Opcenter\Migration\Note;

	class User extends Note
	{
		private $original;

		/**
		 * Notify task username will change on destination
		 *
		 * @param string|null $user
		 */
		public function setNewUser(?string $user): void
		{
			$this->original = $this->note;
			$this->note = $user;
		}

		/**
		 * Get new username
		 *
		 * @return string
		 */
		public function getNewUser(): string
		{
			return $this->note;
		}

		/**
		 * Get user from backup
		 *
		 * @return string
		 */
		public function getOriginalUser(): string
		{
			return $this->original ?? $this->note;
		}

		/**
		 * User has changed via override
		 *
		 * @return bool
		 */
		public function hasChange(): bool
		{
			return $this->getOriginalUser() !== $this->note;
		}
	}