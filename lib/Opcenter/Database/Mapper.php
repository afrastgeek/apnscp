<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */


	namespace Opcenter\Database;

	use Opcenter\Map;
	use Opcenter\Provisioning\Traits\MapperTrait;

	class Mapper
	{
		use MapperTrait;
		protected $map;

		public function __construct(string $dbtype, string $maptype)
		{
			if ($dbtype !== 'pgsql' && $dbtype !== 'mysql') {
				fatal("unsupported database map type `%s'", $dbtype);
			}
			if ($maptype !== 'user' && $maptype !== 'prefix') {
				fatal("unknown database map type `%s'", $maptype);
			}
			$this->map = "${dbtype}.${maptype}map";
		}

		public function map(string $key, $val): bool
		{
			return static::mapWrap('add', $val, $key, $this->map);
		}

		public function unmap(string $key, string $challenge = ''): bool
		{
			return static::mapWrap('remove', $key, $challenge, $this->map);
		}

		public function get(string $key)
		{
			return Map::load($this->map, 'r')->fetch($key);
		}


	}