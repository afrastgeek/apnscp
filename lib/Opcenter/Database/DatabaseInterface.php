<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Database;

	interface DatabaseInterface
	{
		public static function prefixExists(string $prefix): bool;

		/**
		 * Move a database
		 *
		 * @param string $old
		 * @param string $new
		 * @return bool
		 */
		public static function moveDatabase(string $old, string $new): bool;

		/**
		 * PostgreSQL User exists
		 *
		 * @param string $user
		 * @return bool
		 */
		public static function userExists(string $user): bool;

		/**
		 * Rename a PostgreSQL user
		 *
		 * @param string $old old name
		 * @param string $new new name
		 * @return bool
		 */
		public static function renameUser(string $old, string $new): bool;

		public static function flushTables(): bool;

		/**
		 * Verify if database exists
		 *
		 * @param string      $dbname database name
		 * @param string|null $user   optional user to confirm against
		 * @return bool
		 */
		public static function databaseExists(string $dbname, string $user = null): bool;

		/**
		 * Drop a database
		 *
		 * @param string $dbname database name to drop
		 * @return bool
		 */
		public static function dropDatabase(string $dbname): bool;

		/**
		 * Delete a user ("role")
		 *
		 * @param string $user
		 * @param string $hostname
		 * @return bool
		 */
		public static function deleteUser(string $user, string $hostname);

		public static function createUser(string $user, string $password, string $hostname = 'localhost'): bool;

		/**
		 * Dump the tablespace
		 *
		 * @param string $user
		 * @param string $host
		 * @return bool
		 */
		public static function deleteMainUser(string $user, string $host = null);
	}