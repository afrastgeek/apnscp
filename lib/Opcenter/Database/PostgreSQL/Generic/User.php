<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database\PostgreSQL\Generic;

	class User
	{
		public function renameAdminUser(string $old, string $new): string
		{
			return "UPDATE siteinfo SET admin_user = '" . pg_escape_string($new) . "' WHERE admin_user = '" . pg_escape_string($old) . "'";
		}

		/**
		 * Rename user by user ID
		 *
		 * @param int      $uid
		 * @param string   $new
		 * @param int|null $site_id optional site ID sanity check
		 * @return string
		 */
		public function renameUser(int $uid, string $new, int $site_id = null): string
		{
			$query = 'UPDATE uids SET "user" = \'' . pg_escape_string($new) .
				'\' WHERE uid = ' . $uid;
			if ($site_id) {
				$query .= ' AND site_id = ' . $site_id;
			}

			return $query;
		}

		public function saveGid(int $site_id, int $gid): string
		{
			return "INSERT INTO gids (site_id, gid) VALUES($site_id, $gid)";
		}

		public function saveUid(int $site_id, int $uid, string $name): string
		{
			return "INSERT INTO uids(site_id, uid, \"user\") " .
				"VALUES(${site_id}, ${uid}, '" . pg_escape_string($name) . "')";
		}

		public function deleteUid(int $site_id, int $uid): string
		{
			return "DELETE FROM uids WHERE site_id = ${site_id} AND uid = ${uid}";
		}

		public function deleteGid(int $site_id, int $gid = null): string
		{
			$query = "DELETE FROM gids WHERE site_id = ${site_id}";
			if ($gid) {
				$query .= " AND gid = ${gid}";
			}
			return $query;
		}

	}